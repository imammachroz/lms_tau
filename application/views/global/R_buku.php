<head>
    <style>
        
        /* EXAMPLE 6 - Background Logo*/
        .example6 .navbar-brand{ 
        background: url("assets/images/tau_logo.png") center / contain no-repeat;
        /* background: url("assets/images/tau logo150x150.png") center / contain no-repeat; */
        width: 200px;
        }
    </style>
</head>

<div class="row">
    <div class="col-md-12">
        <h4 class="page-head-line" style="margin-top: 75px;text-align:center">Data Buku Perpustakaan</h4>
     </div>

                      <div class="col-md-12">
                        
                        
                                <div class="panel-body">
                                <ul>

                                <div class="row">
    <div class="col-md-12">
      <form action="<?=site_url('Buku/list_buku');?>" method="get">
        <div class="panel panel-primary">
            <div class="panel-heading">Filter</div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <td width="20%">Pilih Kategori Koleksi</td>
                        <td width="30%">
                            <select class="form-control" name="filter_kategori">
                                <option value="" <?php if(empty($this->input->get('filter_kategori'))) echo 'selected';?>>Show All data</option>
                                <?php
                                foreach($kategori as $category)
                                {
                                    $selected = $this->input->get('filter_kategori')==$category['id_kategori'] ? 'selected':'';
                                    echo '<option value="'.$category['id_kategori'].'" '.$selected.'>'.$category['nama_kategori'].'</option>';
                                }
                                ?> 
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer text-right">
                <button type="submit" class="btn btn-primary">Tampilkan Data</button>
            </div>
        </div>
      </form>
    </div>
  </div>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">

   <div class="form-group"></div>
   <table id="table-buku" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>ISBN</th>
                <th>Judul Buku</th>
                <th>Kelas</th>
                <th>Kategori</th>
                <th>Penerbit</th>
                <th>Pengarang</th>
                <th>Stok Tersedia</th>
            </tr>
        </thead>
        <tbody>
         <?php
  $no = 1;
    foreach($data_buku->result_array() as $op)
    {
    ?>
            <tr>
                <td><?php echo $no++ ;?></td>
                <td><?php echo $op['isbn'];?></td>
                <td><?php echo $op['judul_buku'];?></td>
                <td><?php echo $op['nama_kelas'];?></td>
                <td><?php echo $op['nama_kategori'];?></td>
                <td><?php echo $op['penerbit'];?></td>
                <td><?php echo $op['penulis'];?></td>
                <td><?php echo $op['stok_tersedia'];?></td>
            </tr>
<?php
    }
  ?>            
         </tbody>
    </table>
  </div>
  <div class="box-footer">
    Lakukan pencarian Buku pada form <b>search</b> .
  </div><!-- box-footer -->
</div><!-- /.box --></ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>