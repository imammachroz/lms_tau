<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Buku_model
class Pengembalian_model extends CI_Model
{
	// Property yang bersifat public   
    public $table = 'peminjaman';
    public $id = 'id_peminjaman';
    public $order = 'DESC';
    
    
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
    }
	
	
    // Menampilkan semua data 
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
   
    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // Merubah data kedalam database
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // Menghapus data kedalam database
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    
    function pengembalian ($id) {
        $denda = $denda;
        $this->db->set('is_kembali','Y');
        $this->db->set('denda',$denda );
        $this->db->where($this->id, $id);
        $this->db->update($this->table);
    }

    function pengembalian_denda ($id, $denda ){
        $this->db->set('is_kembali','Y');
        $this->db->set('denda',$denda );
        $this->db->where($this->id, $id);
        $this->db->update($this->table);
    }
    function update_peminjaman ($id) {
        $this->db->set('tanggal_kembali', adddate (now+3));
        $this->db->where($this->id, $id);
        $this->db->update($this->table);
    }
    


}
