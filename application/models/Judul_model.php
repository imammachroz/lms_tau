<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Judul_model
class Judul_model extends CI_Model
{
	// Property yang bersifat public   
    public $table = 'judul';
    public $id = 'id_judul';
    public $order = 'DESC';
    
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
    }
	
	// Tabel data dengan nama judul
    function json() {		
        $this->datatables->select("id_judul,isbn,judul_buku,penulis,penerbit,id_kelas,id_kategori");
        $this->datatables->from('judul');        
        $this->datatables->add_column('action', anchor(site_url('judul/read/$1'),'<button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button>'), 'id_judul');
        return $this->datatables->generate();
    }
	
    // Menampilkan semua data 
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // menampilkan jumlah data	
    function total_rows($q = NULL) {
        $this->db->like('id_judul', $q);
		$this->db->or_like('id_judul', $q);
		$this->db->or_like('judul_buku', $q);
		$this->db->or_like('penulis', $q);
		$this->db->or_like('penerbit', $q);
		$this->db->or_like('id_kelas', $q);
		$this->db->or_like('id_ketegori', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Menampilkan data dengan jumlah limit
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_judul', $q);
		$this->db->or_like('id_judul', $q);
		$this->db->or_like('judul_buku', $q);
		$this->db->or_like('penulis', $q);
		$this->db->or_like('penerbit', $q);
		$this->db->or_like('id_kelas', $q);
		$this->db->or_like('id_ketegori', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // Merubah data kedalam database
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // Menghapus data kedalam database
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}