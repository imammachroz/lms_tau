<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penulis extends CI_Controller
{
     // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Penulis_model'); // Memanggil Penulis_model yang terdapat pada models
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library 
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper		
		$this->load->helper('my_function'); // Memanggil fungsi my_function yang terdapat pada helper		
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman penulis
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('penulis/penulis_list'); // Menampilkan halaman utama penulis
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Penulis_model->json();
    }
	
	/*
    public function read($id) 
    {
        $row = $this->Penulis_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_penulis' => $row->id_penulis,
		'nama_penulis' => $row->nama_penulis,
		'penulis_seo' => $row->penulis_seo,
		'aktif' => $row->aktif,
	    );
            $this->load->view('penulis/penulis_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penulis'));
        }
    }
	*/
	
    public function create() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
        $data = array(
			'back'   => site_url('penulis'),
            'button' => 'Create',
            'action' => site_url('penulis/create_action'),
			'id_penulis' => set_value('id_penulis'),
			'nama_penulis' => set_value('nama_penulis'),		
		);
		
		$this->load->view('header',$dataAdm ); // Menampilkan bagian header dan object data users 	 
        $this->load->view('penulis/penulis_form', $data); // Menampilkan halaman form penulis
		$this->load->view('footer'); // Menampilkan bagian footer
    }
    
    public function create_action() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } 
		else {
            $data = array(
				'id_penulis'   => $this->input->post('id_penulis',TRUE),
				'nama_penulis' => $this->input->post('nama_penulis',TRUE),
				);           
            $this->Penulis_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penulis'));
        }
		
    }
    
    public function update($id) 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
        $row = $this->Penulis_model->get_by_id($id);

        if ($row) {
            $data = array(
				'back'   => site_url('penulis'),
                'button' => 'Update',
                'action' => site_url('penulis/update_action'),
				'id_penulis' => set_value('id_penulis', $row->id_penulis),
				'nama_penulis' => set_value('nama_penulis', $row->nama_penulis),
				);
							
			$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 	
            $this->load->view('penulis/penulis_form', $data); // Menampilkan form mahasiswa
			$this->load->view('footer'); // Menampilkan bagian footer
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penulis'));
        }
    }
    
    public function update_action() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_penulis', TRUE));
        } else {
            $data = array(
						'id_penulis'   => $this->input->post('id_penulis',TRUE),
						'nama_penulis' => $this->input->post('nama_penulis',TRUE),
						);
			
            $this->Penulis_model->update($this->input->post('id_penulis', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penulis'));
        }
    }
    
    public function delete($id) 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $row = $this->Penulis_model->get_by_id($id);

        if ($row) {
            $this->Penulis_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penulis'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penulis'));
        }
    }

    public function _rules(){
	$this->form_validation->set_rules('id_penulis', 'id penulis', 'trim|required');
	$this->form_validation->set_rules('nama_penulis', 'nama penulis', 'trim|required');
	$this->form_validation->set_rules('id_penulis', 'id_penulis', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
?>