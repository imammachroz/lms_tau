<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Users</a></li>
        <li class="active"><?php echo $button ?> Users</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input atau edit Users -->
			<h2 style="margin-top:0px">Users <?php echo $button ?></h2>
			<form action="<?php echo $action; ?>" method="post">
				<div class="form-group">
					<label for="varchar">Username <?php echo form_error('username') ?></label>
					<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" readonly />
				</div>
				<div class="form-group">
					<label for="varchar">Password<?php echo form_error('password') ?></label>
					<div class="input-group">
						<input type="password" class="form-control pwd" name="password" id="password" placeholder="Password"  onclick="this.value=''" value="<?php echo $password; ?>"  />
						<span class="input-group-btn">
							<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
						</span>
					</div>
				</div>
				<!-- untuk show password -->
				<!-- <div class="form-group">
					<input type="checkbox" onclick="myFunction()"> Show Password
				</div> -->
					
				<div class="form-group">
					<label for="varchar">Email <?php echo form_error('email') ?></label>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
				</div>
				<?php
					if($level == 'admin'){
				?>
					<div class="form-group">
					<label for="varchar">Password Email<?php echo form_error('email_password') ?></label>
					<div class="input-group">
					<input type="password" class="form-control" name="email_password" id="email_password" placeholder="Email Password" onclick="this.value=''" value="<?php echo $email_password; ?>" />
						<span class="input-group-btn">
							<button class="btn btn-default reveal_email" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
						</span>
					</div>
				</div>
				<div class="form-group">
					<input type="hidden" class="form-control" name="level" id="level" placeholder="Level" value="<?php echo $level; ?>" />         
				</div>
				<div class="form-group">
					<input type="hidden" class="form-control" name="blokir" id="blokir" placeholder="Blokir" value="<?php echo $blokir; ?>" />         
				</div>
				<?php
					}
				?>
				<?php
					if($level == 'user'){
				?>			
				<div class="form-group">
					<label for="enum">Level <?php echo form_error('level') ?></label>
					<select name="level" class="form-control select2" style="width: 100%;">		
						<?php
							if($level == 'admin'){
						?>
							<option value="admin" selected>Admin</option>
							<option value="user">Mahasiswa</option>
						<?php
							}
							elseif($level == 'user'){
						?>
							<option value="admin">Admin</option>
							<option value="user" selected>Mahasiswa</option>
						<?php
							}
							else{
						?>
							<option value="">--Pilih--</option>
							<option value="admin">Admin</option>
							<option value="user">Mahasiswa</option>
						<?php
							}
						?>
					</select>            
				</div>
				<?php
					}
				?>
				<?php
					if($level == 'user'){
				?>	
				<div class="form-group">
					<label for="enum">Blokir <?php echo form_error('blokir') ?></label>
					<select name="blokir" class="form-control select2" style="width: 100%;">		
						<?php
							if($blokir == 'Y'){
						?>
							<option value="Y" selected>Ya</option>
							<option value="N">Tidak</option>
						<?php
							}
							elseif($blokir == 'N'){
						?>
							<option value="Y">Ya</option>
							<option value="N" selected>Tidak</option>
						<?php
							}
							else{
						?>
							<option value="Y">Ya</option>
							<option value="N">Tidak</option>
						<?php
							}
						?>
					</select>              
				</div>
				<?php
					}
				?>
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
				<a href="<?php echo site_url('users') ?>" class="btn btn-default">Cancel</a>
			</form>
    