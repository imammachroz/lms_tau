<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Judul
class Judul extends CI_Controller
{
     // Konstruktor			
	function __construct()
    {
        parent::__construct();
        $this->load->model('Judul_model'); // Memanggil Judul_model yang terdapat pada models
        $this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->model('Buku_model');
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper
		$this->load->library('upload'); // Memanggil upload yang terdapat pada helper
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman judul
    public function index(){   
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('judul/judul_list'); // Menampilkan halaman utama judul
		$this->load->view('footer_list'); // Menampilkan bagian footer
    }
	
	// Fungsi JSON
	public function json() {
        header('Content-Type: application/json');
        echo $this->Judul_model->json();
	}
	
	public function search_penulis(){

		$penulis = $this->input->get('penulis');
		$query = $this->Judul_model->get_penulis($penulis,'nama_penulis');
		echo json_encode($query);

	}

	public function search_penerbit(){

		$penerbit = $this->input->get('penerbit');
		$query = $this->Judul_model->get_penerbit($penerbit,'nama_penerbit');
		echo json_encode($query);

	}
	// Fungsi menampilkan form Create Judul
    public function create(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
	  
	  // Menampung data yang diinputkan
      $data = array(
        'button' => 'Create',
		'back'   => site_url('judul'),
        'action' => site_url('judul/create_action'),
	    'id_judul' => set_value('id_judul'),
	    'isbn' => set_value('isbn'),
	    'judul_buku' => set_value('judul_buku'),
	    'id_penulis' => set_value('id_penulis'),
	    'id_penerbit' => set_value('id_penerbit'),
		'cover' => set_value('cover'),
		'id_kelas' => set_value('id_kelas'),
		'id_kategori' => set_value('id_kategori'),
	  );
        $this->load->view('header',$dataAdm ); // Menampilkan bagian header dan object data users 	 
        $this->load->view('judul/judul_form', $data); // Menampilkan halaman form judul
		$this->load->view('footer'); // Menampilkan bagian footer
    }
    
	// Fungsi untuk melakukan aksi simpan data
    public function create_action(){
		
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $this->_rules(); // Rules atau aturan bahwa setiap form harus diisi
		
		// Jika form judul belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } 
		// Jika form judul telah diisi dengan benar 
		// maka sistem akan menyimpan kedalam database
		else {	
			// konfigurasi untuk melakukan upload cover
			$config['upload_path']   = './images/';    //path folder image
			$config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg			
			$config['file_name']     = url_title($this->input->post('id_judul')); //nama file cover dirubah menjadi nama berdasarkan nim	
			$this->upload->initialize($config);
			
			// Jika file cover ada 
			if(!empty($_FILES['cover']['cover'])){
				
				if ($this->upload->do_upload('cover')){
					$cover = $this->upload->data();	
					$datacover = $cover['file_name'];					
					$this->load->library('upload', $config);		    
					
					$data = array(
						'id_judul' => $this->input->post('id_judul',TRUE),
						'isbn' => $this->input->post('isbn',TRUE),
						'judul_buku' => $this->input->post('judul_buku',TRUE),
						'id_penulis' => $this->input->post('id_penulis',TRUE),
						'id_penerbit' => $this->input->post('id_penerbit',TRUE),
						'cover' => $this->input->post('cover',TRUE),
						'id_kelas' => $this->input->post('id_kelas',TRUE),
						'id_kategoty' => $this->input->post('id_kategoty',TRUE),
					
				);
					$this->Judul_model->insert($data);
					
				}
				
				$this->session->set_flashdata('message', 'Create Record Success');
				redirect(site_url('judul'));			
			}
			// Jika file cover kosong 
			else{		
			
				$data = array(
					'id_judul' => $this->input->post('id_judul',TRUE),
					'isbn' => $this->input->post('isbn',TRUE),
					'judul_buku' => $this->input->post('judul_buku',TRUE),
					'id_penulis' => $this->input->post('id_penulis',TRUE),
					'id_penerbit' => $this->input->post('id_penerbit',TRUE),
					'id_kelas' => $this->input->post('id_kelas',TRUE),
					'id_kategori' => $this->input->post('id_kategori',TRUE),
				);            
				
				$this->Judul_model->insert($data);
				$this->session->set_flashdata('message', 'Create Record Success');
				redirect(site_url('judul'));	
			}
					
        }
    }

	public function delete($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $row = $this->Judul_model->get_by_id($id);
		//jika id nim yang dipilih tersedia maka akan dihapus
        if ($row) {
			// menghapus data berdasarkan id-nya yaitu nim
			$this->Judul_model->delete($id);
				// menampilkan informasi 'Delete Record Success' setelah data mahasiswa dihapus 
				$this->session->set_flashdata('message', 'Delete Record Success');
				
				// menghapus file photo
				unlink("../../assets/images/".$row->photo);
			
			// jika data tidak ada yang dihapus maka akan menampilkan 'Can not Delete This Record !'
		
            redirect(site_url('judul'));				
			
        } 
		//jika id nim yang dipilih tidak tersedia maka akan muncul pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('judul'));
        }
    }

	public function update($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampilkan data berdasarkan id-nya yaitu nim
        $row = $this->Judul_model->get_by_id_judul($id);
		
		// Jika id-nya dipilih maka data mahasiswa ditampilkan ke form edit mahasiswa
        if ($row) {
            $data = array(
                'button' => 'Update',
				'back'   => site_url('judul'),
				'action' => site_url('judul/update_action'),
				'id_judul' => set_value('id_judul',$row->id_judul),
				'isbn' => set_value('isbn',$row->isbn),
				'judul_buku' => set_value('judul_buku',$row->judul_buku),
				'id_penulis' => set_value('id_penulis',$row->id_penulis),
				'id_penerbit' => set_value('id_penerbit',$row->id_penerbit),
				'id_kelas' => set_value('id_kelas',$row->id_kelas),
				'id_kategori' => set_value('id_kategori',$row->id_kategori),
				
			);
		    $this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
            $this->load->view('judul/judul_form_1', $data); // Menampilkan form mahasiswa
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika id-nya yang dipilih tidak ada maka akan menampilkan pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('judul'));
        }
	}
	
	public function update_action(){
		
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $this->_rules(); // Rules atau aturan bahwa setiap form harus diisi	 			
		
		// Jika form judul belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_judul', TRUE));
		} 
		
		else {

			$id_judul = $this->input->post('id_judul',TRUE);

        //$dataBuku = $this->Judul_model->get_by_id($id_judul);
        $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
        $dataAdm = array(	
            'wa'       => 'Web administrator',
            'univ'     => 'Library Management System',
            'username' => $rowAdm->username,
            'email'    => $rowAdm->email,
            'level'    => $rowAdm->level,
        );
          // Menampilkan data BUKU
          $data = array(
			'id_kategori' => $this->input->post('id_kategori',TRUE),
			'id_kelas' => $this->input->post('id_kelas',TRUE),
			'isbn' => $this->input->post('isbn',TRUE),
			'judul_buku' => $this->input->post('judul_buku',TRUE),
			'id_penulis' => $this->input->post('id_penulis',TRUE),
			'id_penerbit' => $this->input->post('id_penerbit',TRUE),
		);	 
		$this->Judul_model->update($this->input->post('id_judul',TRUE),$data);
		$this->session->set_flashdata('message', 'Update Record Success');
		redirect(site_url('judul'));
		}
		
		$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
		$this->load->view('judul'); // Menampilkan data KRS
		$this->load->view('footer'); 
    }

    public function baca_buku($id)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login			
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }
	  $this->db->select('b.id_buku,b.kode_buku,b.is_ada,j.judul_buku');
	  $this->db->from('buku as b');
	  $this->db->where('b.id_judul', $id);
	  $this->db->join('judul as j','j.id_judul=b.id_judul');
	  $buku = $this->db->get()->result();
	return $buku;
	}
	
	// Fungsi untuk menampilkan halaman judul secara detail
    public function read($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampilkan data judul yang ada di database berdasarkan id-nya yaitu id_judul
        $row = $this->Judul_model->get_by_id($id);

        $id_judul=$this->input->post('id_judul',TRUE);
        $kode_buku=$this->input->post('kode_buku',true);
        // Menampung data yang diinputkan 	
        $data = array('action' => site_url('judul/judul_action'),
            'id_judul'=>$id_judul,
            );
        
		// Jika data judul tersedia maka akan ditampilkan
        if ($row) {
            $dataBuku=array(
                'button' => 'Create',
                'back'   => site_url('judul'),
                'buku_data'=>$this->baca_buku($id),
                'id_judul'=>$this->Judul_model->get_by_id($id)->id_judul,
                'isbn'=>$this->Judul_model->get_by_id($id)->isbn,			   
                'judul_buku'=>$this->Judul_model->get_by_id($id)->judul_buku,
                'penulis'=>$this->Judul_model->get_by_id($id)->penulis,
				'penerbit'=>$this->Judul_model->get_by_id($id)->penerbit,
				'kelas'=>$this->Judul_model->get_by_id($id)->kelas,
				'kategori'=>$this->Judul_model->get_by_id($id)->kategori,
				
                );
            $this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
			$this->load->view('judul/judul_read', $dataBuku); // Menampilkan halaman detail judul
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika data judul tidak tersedia maka akan ditampilkan informasi 'Record Not Found'
		else {
			$this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
            $this->session->set_flashdata('message', 'Record Not Found');
			$this->load->view('footer'); // Menampilkan bagian footer
            redirect(site_url('judul'));
        }
    }
	
    
	// Fungsi menampilkan form Update Judul
    public function update_buku($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampilkan data berdasarkan id-nya yaitu id_judul
        $row = $this->Buku_model->get_by_id($id);
		// Jika id-nya dipilih maka data judul ditampilkan ke form edit judul
        if ($row) {
            $data = array(
                'button' => 'Update',
				'back'   => site_url('judul'),
				'action' => site_url('judul/update_buku_action'),
				'buku_data'=>$this->baca_buku($id),
				'id_buku' => set_value('id_buku', $row->id_buku),
				'id_judul' => set_value('id_judul', $row->id_judul),
				'kode_buku' => set_value('kode_buku',$row->kode_buku),
				'is_ada' => set_value('is_ada', $row->is_ada),
			);
		    $this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
            $this->load->view('judul/buku_form', $data); // Menampilkan form judul
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika id-nya yang dipilih tidak ada maka akan menampilkan pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('judul'));
        }
    }
    
	// Fungsi untuk melakukan aksi update data
    public function update_buku_action(){
		
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $this->_rules(); // Rules atau aturan bahwa setiap form harus diisi	 			
		
		// Jika form judul belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == TRUE) {
            $this->update($this->input->post('id_buku', TRUE));
		} 
		
		else {
		$id_judul = $this->input->post('id_judul',TRUE);
        $kode_buku = $this->input->post('kode_buku',TRUE);
		$is_ada = $this->input->post('is_ada',TRUE);
		$id_buku = $this->input->post('id_buku',TRUE);

        //$dataBuku = $this->Judul_model->get_by_id($id_judul);
        $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
        $dataAdm = array(	
            'wa'       => 'Web administrator',
            'univ'     => 'Library Management System',
            'username' => $rowAdm->username,
            'email'    => $rowAdm->email,
            'level'    => $rowAdm->level,
        );
          // Menampilkan data BUKU
          $data = array(
			'id_buku' =>$id_buku,
            'kode_buku' => $kode_buku,
            'is_ada' => $is_ada,
        );	 
        $this->Buku_model->update($id_buku,$data);
         //   redirect(site_url('/judul'));
            // Menampilkan data KRS 
         $dataBuku=array(
            'button' => 'Create',
            'back'   => site_url('judul'),
            'buku_data'=>$this->baca_buku($id_judul),
            'id_judul'=>$this->Judul_model->get_by_id($id_judul)->id_judul,
            'isbn'=>$this->Judul_model->get_by_id($id_judul)->isbn,			   
            'judul_buku'=>$this->Judul_model->get_by_id($id_judul)->judul_buku,
            'penulis'=>$this->Judul_model->get_by_id($id_judul)->penulis,
            'penerbit'=>$this->Judul_model->get_by_id($id_judul)->penerbit,
            'kelas'=>$this->Judul_model->get_by_id($id_judul)->kelas,
            'kategori'=>$this->Judul_model->get_by_id($id_judul)->kategori,
            );
            $this->session->set_flashdata('message', 'Create Record Success');
            
      
      $this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
      $this->load->view('judul/judul_read',$dataBuku); // Menampilkan data KRS
      $this->load->view('footer'); 
        }
    }
    
	// Fungsi untuk melakukan aksi delete data berdasarkan id yang dipilih
    public function delete_buku($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		$id_delete=$this->Buku_model->get_by_id($id)->id_judul;
        $row = $this->Buku_model->get_by_id($id);
		
		//jika id nim yang dipilih tersedia maka akan dihapus
        if ($row) {
			// menghapus data berdasarkan id-nya yaitu nim
			if($this->Buku_model->delete($id)){
				
				// menampilkan informasi 'Delete Record Success' setelah data judul dihapus 
				$this->session->set_flashdata('message', 'Can not Delete This Record !');
			}
			// jika data tidak ada yang dihapus maka akan menampilkan 'Can not Delete This Record !'
			else{
			
				$this->session->set_flashdata('message', 'Delete Record Success');	
			}
            redirect(site_url('judul/read/'.$id_delete));				
			
        } 
		//jika id nim yang dipilih tidak tersedia maka akan muncul pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('judul'));
        }
	}
	
// Fungsi menampilkan form Create KRS
public function create_buku($id) 
{
    // Jika session data username tidak ada maka akan dialihkan kehalaman login			
    if (!isset($this->session->userdata['username'])) {
        redirect(base_url("login"));
    }
    
    // Menampilkan data berdasarkan id-nya yaitu username
    $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
    $dataAdm = array(	
        'wa'       => 'Web administrator',
        'univ'     => 'Library Management System',
        'username' => $rowAdm->username,
        'email'    => $rowAdm->email,
        'level'    => $rowAdm->level,
    );
    
    // Menampung data yang diinputkan 
    $data = array(
        'button' => 'Create',
        'judul'=>'Tambah',
        'back'   => site_url('judul'),
        'action' => site_url('judul/create_buku_action'),
        'id_buku' => set_value('id_buku'),
        //'judul_buku' => $this->Judul_model->get_by_id($id)->judul_buku,
        'id_judul' =>$id, //set_value('nim'),
        'kode_buku' => set_value('kode_buku'),
        'is_ada' => set_value('is_ada'),
);
$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
$this->load->view('judul/buku_form', $data); // Menampilkan form KRS
$this->load->view('footer'); // Menampilkan bagian footer
}

// Fungsi untuk melakukan aksi simpan data
public function create_buku_action() 
{
    // Jika session data username tidak ada maka akan dialihkan kehalaman login			
    if (!isset($this->session->userdata['username'])) {
        redirect(base_url("login"));
    }
    
    $this->_rules_buku(); // Rules atau aturan bahwa setiap form harus diisi
    
    // Jika form KRS belum diisi dengan benar 
    // maka sistem akan meminta user untuk menginput ulang
    if ($this->form_validation->run() == FALSE) {
        $this->create_buku($this->input->post('id_judul',TRUE)
          );
          // Menampilkan data berdasarkan id-nya yaitu username
         
    } 
    // Jika form KRS telah diisi dengan benar 
    // maka sistem akan menyimpan kedalam database
    else {	
        $id_judul = $this->input->post('id_judul',TRUE);
        $kode_buku = $this->input->post('kode_buku',TRUE);
		$is_ada = $this->input->post('is_ada',TRUE);
		$id_buku = $this->input->post('id_buku',TRUE);

        //$dataBuku = $this->Judul_model->get_by_id($id_judul);
        $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
        $dataAdm = array(	
            'wa'       => 'Web administrator',
            'univ'     => 'Library Management System',
            'username' => $rowAdm->username,
            'email'    => $rowAdm->email,
            'level'    => $rowAdm->level,
        );
          // Menampilkan data KRS
          $data = array(
            'id_judul' => $id_judul,
            'kode_buku' => $kode_buku,
            'is_ada' => $is_ada,
		);	 
		
		$this->Buku_model->insert($data);
		$this->session->set_flashdata('message', 'Create Record Success');
        
         //   redirect(site_url('/judul'));
            // Menampilkan data KRS 
         $dataBuku=array(
            'button' => 'Create',
            'back'   => site_url('judul'),
            'buku_data'=>$this->baca_buku($id_judul),
            'id_judul'=>$this->Judul_model->get_by_id($id_judul)->id_judul,
            'isbn'=>$this->Judul_model->get_by_id($id_judul)->isbn,			   
            'judul_buku'=>$this->Judul_model->get_by_id($id_judul)->judul_buku,
            'penulis'=>$this->Judul_model->get_by_id($id_judul)->penulis,
            'penerbit'=>$this->Judul_model->get_by_id($id_judul)->penerbit,
			);
			redirect(site_url('judul/read/'.$id_judul));
            
      
      $this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
      $this->load->view('judul/judul_read',$dataBuku); // Menampilkan data KRS
      $this->load->view('footer'); 
    }
}
	
	// Fungsi rules atau aturan untuk pengisian pada form (create/input dan update)
    public function _rules() 
    {
	// $this->form_validation->set_rules('id_judul', 'id_judul', 'trim');
	$this->form_validation->set_rules('isbn', 'isbn', 'trim|required');
	$this->form_validation->set_rules('judul_buku', 'judul_buku', 'trim|required');
	$this->form_validation->set_rules('id_kelas', 'id kelas', 'trim|required|is_natural_no_zero');
	$this->form_validation->set_rules('id_kategori','id kategori', 'trim|required|is_natural_no_zero');
	$this->form_validation->set_rules('id_penulis', 'id penulis', 'trim|required|is_natural_no_zero');
	$this->form_validation->set_rules('id_penerbit', 'id_penerbit', 'trim|required|is_natural_no_zero');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
	public function _rules_buku() 
    {
	$this->form_validation->set_rules('kode_buku', 'kode_buku', 'trim|required');
	$this->form_validation->set_rules('is_ada', 'is_ada', 'trim|required');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
?>