<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="../admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $button ?> Users</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input atau edit Users -->
			<h2 style="margin-top:0px">Users <?php echo $button ?></h2>
			<form action="<?php echo $action; ?>" method="post">
			<div class="form-group">
					<input type="hidden" class="form-control" name="level" id="level" placeholder="Level" value="<?php echo $level; ?>" />         
				</div>
				<div class="form-group">
					<input type="hidden" class="form-control" name="blokir" id="blokir" placeholder="Blokir" value="<?php echo $blokir; ?>" />         
				</div>
				<div class="form-group">
					<label for="varchar">Username <?php echo form_error('username') ?></label>
					<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" readonly/>
				</div>
				<div class="form-group">
					<label for="varchar">Password<?php echo form_error('password') ?></label>
					<div class="input-group">
						<input type="password" class="form-control pwd" name="password" id="password" placeholder="Password"  onclick="this.value=''" value="<?php echo $password; ?>"  />
						<span class="input-group-btn">
							<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label for="varchar">Email <?php echo form_error('email') ?></label>
					<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
				</div>		
				
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
				<a href="<?php echo site_url('admin') ?>" class="btn btn-default">Cancel</a>
			</form>
    