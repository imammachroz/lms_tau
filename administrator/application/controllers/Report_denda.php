<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_denda extends CI_Controller
{
     // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Report_model'); // Memanggil Kategori_model yang terdapat pada models
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library 
        $this->load->library('pdf');
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper		
		$this->load->helper('my_function'); // Memanggil fungsi my_function yang terdapat pada helper		
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
    
    
    public function index(){
        // Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
        );
        // $data_peminjaman = $this->Report_model->getAllPeminjamanData();
        // $data = array(
        //     'data'  =>  $data_peminjaman,
        // );
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('report/report_list_denda'); // Menampilkan halaman utama kategori
		$this->load->view('footer_list'); // Menampilkan bagian footer
    }
    public function json() {
        header('Content-Type: application/json');
        echo $this->Report_model->json_report_denda();
    }

    public function search(){
		$valid = $this->form_validation;
		$valid->set_error_delimiters('<i style="color: red;">', '</i>');
        $valid->set_rules('start_date', 'Field Tanggal Pinjam', 'required|trim|strip_tags|htmlspecialchars');
		$valid->set_rules('end_date', 'Field Tanggal Kembali', 'required|trim|strip_tags|htmlspecialchars');
		
		if ($valid->run() === TRUE)
        {
			$input = $this->input->post(NULL, TRUE);
			$data = $this->Report_model->filter_tanggal_denda($input["start_date"], $input["end_date"]);
			return $this->response([
                    'data' => array_values($data)
        	]);
		} else return  $this->response(['success' => FALSE, 'error' => validation_errors()]);
    }
    
    public function response($data)
    {
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit();
    }
}
?>