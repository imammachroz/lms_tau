<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Konfigurasi_peminjaman_model
class Konfigurasi_peminjaman_model extends CI_Model
{
   
    // Property yang bersifat public   
    public $table = 'konfigurasi_peminjaman';
    public $id = 'id_peminjam';
    public $order = 'DESC';
    
   // Konstrutor   
   function __construct()
    {
        parent::__construct();
    }

    // Tabel data dengan nama konfigurasi_peminjaman
    function lama_hari($id_mahasiswa) {
        $this->db->select('jumlah_hari');
        $this->db->from('konfigurasi_peminjaman');
        $this->db->join('mahasiswa','mahasiswa.id_peminjam=konfigurasi_peminjaman.id_peminjam');
        $this->db->where('mahasiswa.id_mahasiswa',$id_mahasiswa);
        return $this->db->get()->row();
    }

   
   // Menampilkan semua data 
   function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // menampilkan jumlah data	
    function total_rows($q = NULL) {
        $this->db->like('username', $q);
		$this->db->or_like('username', $q);
		$this->db->or_like('password', $q);
		$this->db->or_like('email', $q);
		$this->db->or_like('level', $q);
		$this->db->or_like('blokir', $q);
		$this->db->or_like('id_sessions', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Menampilkan data dengan jumlah limit
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('username', $q);
		$this->db->or_like('username', $q);
		$this->db->or_like('password', $q);
		$this->db->or_like('email', $q);
		$this->db->or_like('level', $q);
		$this->db->or_like('blokir', $q);
		$this->db->or_like('id_sessions', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // Merubah data kedalam database
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // Menghapus data kedalam database
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Konfigurasi_peminjaman_model.php */
/* Location: ./application/models/Konfigurasi_peminjaman_model.php */
/* Please DO NOT modify this information : */