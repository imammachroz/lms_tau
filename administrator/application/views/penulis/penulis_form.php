<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Penulis</a></li>
        <li class="active"><?php echo $button ?> Penulis</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
		<!-- Form input dan edit Penulis-->
		<legend><?php echo $button ?> Penulis</legend>
        <form action="<?php echo $action; ?>" method="post">
		<input type="hidden" class="form-control" name="id_penulis" id="id_penulis" value="<?php echo $id_penulis; ?>" />
	    <div class="form-group">
            <label for="varchar">Nama Penulis <?php echo form_error('nama_penulis') ?></label>
            <input type="text" class="form-control" name="nama_penulis" id="nama_penulis" placeholder="Nama Penulis" value="<?php echo $nama_penulis; ?>" />
        </div>	
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('penulis') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>