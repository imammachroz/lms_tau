<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
		<li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="<?=base_url()?>peminjaman">Peminjaman</a></li>
        <li class="active">Anggota</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Menampilkan Data Mahasiswa -->
			<div class="row" style="margin-bottom: 10px">
				<div class="col-md-4">
					<h2 style="margin-top:0px">Anggota</h2>
				</div>
				<div class="col-md-4 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
				<div class="col-md-4 text-right">
					<?php echo anchor(site_url('peminjaman'), 'Cancel', 'class="btn btn-warning"'); ?>
				</div>
			</div>
			<table class="table table-bordered table-striped" id="mytable">
				<thead>
					<tr>
						<th width="80px">No</th>
						<th>NIM</th>
						<th>Nama Lengkap</th>
						<th>Email</th>
						<th>Telp</th>
						<th width="80px">Action</th>
					</tr>
				</thead>
			
			</table>

			<script src="../assets/js/resp/jquery-3.3.1.js"></script>
			<script src="../assets/js/resp/jquery.dataTables.min.js"></script>
			<script src="../assets/js/resp/dataTables.rowReorder.min.js"></script>
			<script src="../assets/js/resp/dataTables.responsive.min.js"></script>
			<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
			
			<link rel="stylesheet" type="text/css" href="../assets/css/rowReorder.dataTables.min.css"/>
			<link rel="stylesheet" type="text/css" href="../assets/css/responsive.dataTables.min.css"/>
			
			<!-- Memanggil jQuery data tables -->
			<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
			<!-- Memanggil Bootstrap data tables -->
			<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
			
			<!-- JavaScript yang berfungsi untuk menampilkan data dari tabel mahasiswa dengan AJAX -->
			<script type="text/javascript">
				$(document).ready(function() {
					$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
					{
						return {
							"iStart": oSettings._iDisplayStart,
							"iEnd": oSettings.fnDisplayEnd(),
							"iLength": oSettings._iDisplayLength,
							"iTotal": oSettings.fnRecordsTotal(),
							"iFilteredTotal": oSettings.fnRecordsDisplay(),
							"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
							"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
						};
					};
					
					var JK = "jenis_kelamin"; 
					if (JK == "L") {
						tampilJK = "Laki-laki";					
					}
					else{
						tampilJK = "Laki-laki";	
					}
					
					
					var t = $("#mytable").dataTable({
						initComplete: function() {
							var api = this.api();
							$('#mytable_filter input')
									.off('.DT')
									.on('keyup.DT', function(e) {
										if (e.keyCode == 13) {
											api.search(this.value).draw();
										}
							});
						},
						dom: '<lf<tr>ipB>',
                    oLanguage: {
                        sProcessing: "<div id='loader'></div>"
                    },
						bAutoWidth : false, 
						responsive: true,
						rowReorder: {
								selector: 'td:nth-child(0)'
						},
						processing: true,
						serverSide: true,
						ajax: {"url": "./json_peminjaman_mahasiswa", "type": "POST"},
						columns: [
							{
								"data": "nim",
								"orderable": false
							},
								{"data": "nim"},
								{"data": "nama_lengkap"},
								{"data": "email"},
								{"data": "telp"},
							{
								"data" : "action",
								"orderable": false,
								"className" : "text-center"
							}
						],
						order: [[0, 'desc']],
						rowCallback: function(row, data, iDisplayIndex) {
							var info = this.fnPagingInfo();
							var page = info.iPage;
							var length = info.iLength;
							var index = page * length + (iDisplayIndex + 1);
							$('td:eq(0)', row).html(index);
						}
					});
				});
			</script>
		<!--// Tampil Data Mahasiswa -->    