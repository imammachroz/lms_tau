<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Admin
class Admin extends CI_Controller {
	
	// Konstrutor 
	function __construct() {
		parent::__construct();
		$this->load->model('Users_model');
		$this->load->model('Peminjaman_model');
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	}
	
	// Fungsi untuk menampilkan halaman utama admin
	public function index() {
		// Menampilkan data berdasarkan id-nya yaitu username
		$row = $this->Users_model->get_by_id($this->session->userdata['username']);
		$data = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $row->username,
			'email'    => $row->email,
			'level'    => $row->level,
		);
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$pass_email   = decrypt_pass($rowAdm->email_password);
		// Konfigurasi email
        $config = [
			'smtp_timeout'=>'30',
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_user' => "$rowAdm->email",    // Ganti dengan email gmail kamu
			'smtp_pass' => "$pass_email",      // Password gmail kamu
			'smtp_port' => 465,
			'crlf'      => "\r\n",
			'newline'   => "\r\n"
		];

		// Load library email dan konfigurasinya
		$this->load->library('email', $config);
		$this->email->initialize($config);

		$year=date('Y');
		$data['januari']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-01-01', ''.$year.'-01-31')->total;
		$data['februari']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-02-01', ''.$year.'-02-31')->total;
		$data['maret']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-03-01', ''.$year.'-03-31')->total;
		$data['april']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-04-01', ''.$year.'-04-31')->total;
		$data['mei']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-05-01', ''.$year.'-05-31')->total;
		$data['juni']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-06-01', ''.$year.'-06-31')->total;
		$data['juli']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-07-01', ''.$year.'-07-31')->total;
		$data['agustus']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-08-01', ''.$year.'-08-31')->total;
		$data['september']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-09-01', ''.$year.'-09-31')->total;
		$data['oktober']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-10-01', ''.$year.'-10-31')->total;
		$data['november']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-11-01', ''.$year.'-11-31')->total;
		$data['desember']=$this->Peminjaman_model->get_jml_peminjaman(''.$year.'-12-01', ''.$year.'-12-31')->total;
		
		//line chart
		$data['buku_pinjam']=$this->Peminjaman_model->list_buku_pinjam();
		$data['kategori_pinjam']=$this->Peminjaman_model->list_kategori_pinjam();
		$data['kelas_pinjam']=$this->Peminjaman_model->list_kelas_pinjam();

		$data['warna']=	array('#f56954','#00a65a','#f39c12','#00c0ef','#3c8dbc','#d2d6de', '#d9f442','#561d84','#f90ec3', '#31f7a4');
		$this->load->view('beranda',$data);

		//$this->load->view('layout',$tmp);
		
		//$this->load->view('beranda',$data); // Menampilkan halaman utama admin
		
	}
	
	// Fungsi melakukan logout
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('..'));
	}
}
?>