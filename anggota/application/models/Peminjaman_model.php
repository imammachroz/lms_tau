<?php
/**********************************************************/
/* File        : Judul_model.php                      */
/* Lokasi File : ./application/models/Judul_model.php  */
/* Copyright   : Yosef Murya & Badiyanto                  */
/* Publish     : Penerbit Langit Inspirasi                */
/*--------------------------------------------------------*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Judul_model
class Peminjaman_model extends CI_Model
{
	// Property yang bersifat public   
    public $table = 'peminjaman';
    public $table_mahasiswa = 'mahasiswa';
    public $id = 'id_mahasiswa';
    public $id_peminjaman = 'id_peminjaman';
    
    public $order = 'DESC';
    
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
    }
    

    // Tabel data dengan nama judul

    function json() {
            
        $this->datatables->select('peminjaman.id_peminjaman,
        mahasiswa.id_mahasiswa,mahasiswa.nim, mahasiswa.nama_lengkap,
        buku.kode_buku,judul.judul_buku,peminjaman.tanggal_pinjam,
        peminjaman.tanggal_kembali,peminjaman.is_kembali'); 
        //$this->datatables->select('id_peminjaman, id_peminjam,
        //id_mahasiswa, id_buku,tanggal_pinjam,tanggal_kembali');
        $this->datatables->from('peminjaman');
        $this->datatables->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->datatables->join('buku','buku.id_buku=peminjaman.id_buku');
        $this->datatables->join('judul','buku.id_judul=judul.id_judul');
        $this->datatables->group_by('mahasiswa.id_mahasiswa');
        $this->datatables->add_column('action', anchor(site_url('peminjaman/read/$1'),'<button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button>'),'id_mahasiswa');
        return $this->datatables->generate();
    }

    function json1() {	
        $where_clause="mahasiswa.jumlah_buku_pinjam > 0 ";	
        $this->datatables->select('peminjaman.id_peminjaman,
        mahasiswa.id_mahasiswa,mahasiswa.nim, mahasiswa.nama_lengkap,
        peminjaman.tanggal_pinjam, peminjaman.tanggal_kembali,peminjaman.is_kembali'); 
        //$this->datatables->select('id_peminjaman, id_peminjam,
        //id_mahasiswa, id_buku,tanggal_pinjam,tanggal_kembali');
           $this->datatables->from('mahasiswa');
           $this->datatables->join('peminjaman','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
           $this->datatables->where($where_clause, NULL, FALSE);
           $this->datatables->group_by('mahasiswa.id_mahasiswa');
           $this->datatables->add_column('action', anchor(site_url('peminjaman/read/$1'),'<button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button>'),'id_mahasiswa');
           return $this->datatables->generate();
       }
	
    // Menampilkan semua data 
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // function get_buku1($buku, $column)
    // {
    //     $this->db->select('buku.id_buku, buku.kode_buku, judul.judul_buku');
    //     $this->db->from('buku');
    //     $this->db->join('judul','judul.id_judul=buku.id_judul');
    //     $this->db->where('buku.is_ada','Tersedia');
    //     $this->db->like('buku.kode_buku', $buku);
    //     $this->db->like(' judul.judul_buku', $buku);
    //     return $this->db->get()->result_array();
    // }

    function get_buku($buku, $column) {	
        $where_clause="buku.is_ada ='Tersedia' and ( buku.kode_buku like '%$buku%' or judul.judul_buku like '%$buku%' ) ";	
        $this->db->select('buku.id_buku, buku.kode_buku, judul.judul_buku');
        $this->db->from('buku');
        $this->db->join('judul','judul.id_judul=buku.id_judul');
        $this->db->where($where_clause, NULL, FALSE);
        return $this->db->get()->result_array();
       }

    

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table_mahasiswa)->row();
    }
    
    function get_by_id_peminjaman($id_peminjaman)
    {
        $this->db->where($this->id_peminjaman, $id_peminjaman);
        return $this->db->get($this->table)->row();
    }


    // menampilkan jumlah data	
    function total_rows($q = NULL) {
        $this->db->like('id_judul', $q);
		$this->db->or_like('id_judul', $q);
		$this->db->or_like('judul_buku', $q);
		$this->db->or_like('penulis', $q);
		$this->db->or_like('penerbit', $q);
		$this->db->or_like('id_kelas', $q);
		$this->db->or_like('id_ketegori', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Menampilkan data dengan jumlah limit
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_judul', $q);
		$this->db->or_like('id_judul', $q);
		$this->db->or_like('judul_buku', $q);
		$this->db->or_like('penulis', $q);
		$this->db->or_like('penerbit', $q);
		$this->db->or_like('id_kelas', $q);
		$this->db->or_like('id_ketegori', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }
    
    // Merubah data kedalam database
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // Menghapus data kedalam database
    function delete1($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Judul_model.php */
/* Location: ./application/models/Judul_model.php */
/* Please DO NOT modify this information : */