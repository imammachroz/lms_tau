<?php
/**********************************************************/
/* File        : Judul_model.php                      */
/* Lokasi File : ./application/models/Judul_model.php  */
/* Copyright   : Yosef Murya & Badiyanto                  */
/* Publish     : Penerbit Langit Inspirasi                */
/*--------------------------------------------------------*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Judul_model
class Judul_model extends CI_Model
{
	// Property yang bersifat public   
    public $table = 'judul';
    public $id = 'id_judul';
    public $order = 'DESC';
    
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
    }
	
	// Tabel data dengan nama judul
    function json() {		
        $this->datatables->select("id_judul,isbn,judul_buku,penulis.nama_penulis as penulis
        ,penerbit.nama_penerbit as penerbit,kelas.nama_kelas as kelas
        ,kategori.nama_kategori as kategori");
        $this->datatables->from('judul');   
        $this->datatables->join('penulis','judul.id_penulis=penulis.id_penulis');        
        $this->datatables->join('penerbit','judul.id_penerbit=penerbit.id_penerbit');        
        $this->datatables->join('kelas','judul.id_kelas=kelas.id_kelas');
        $this->datatables->join('kategori','judul.id_kategori=kategori.id_kategori');
        $this->datatables->add_column('action', anchor(site_url('judul/read/$1'),'<button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button>'), 'id_judul');
        return $this->datatables->generate();
    }
	
    // Menampilkan semua data 
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($id)
    {
        $this->db->select("id_judul,isbn,judul_buku,penulis.nama_penulis as penulis
        ,penerbit.nama_penerbit as penerbit,kelas.nama_kelas as kelas
        ,kategori.nama_kategori as kategori");
        $this->db->from('judul');   
        $this->db->join('penulis','judul.id_penulis=penulis.id_penulis');        
        $this->db->join('penerbit','judul.id_penerbit=penerbit.id_penerbit');        
        $this->db->join('kelas','judul.id_kelas=kelas.id_kelas');
        $this->db->join('kategori','judul.id_kategori=kategori.id_kategori');
        $this->db->where($this->id, $id);
        return $this->db->get()->row();
    }
    
    // menampilkan jumlah data	
    function total_rows($q = NULL) {
        $this->db->like('id_judul', $q);
		$this->db->or_like('id_judul', $q);
		$this->db->or_like('judul_buku', $q);
		$this->db->or_like('penulis', $q);
		$this->db->or_like('penerbit', $q);
		$this->db->or_like('id_kelas', $q);
		$this->db->or_like('id_ketegori', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Menampilkan data dengan jumlah limit
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_judul', $q);
		$this->db->or_like('id_judul', $q);
		$this->db->or_like('judul_buku', $q);
		$this->db->or_like('penulis', $q);
		$this->db->or_like('penerbit', $q);
		$this->db->or_like('id_kelas', $q);
		$this->db->or_like('id_ketegori', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // Merubah data kedalam database
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // Menghapus data kedalam database
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Judul_model.php */
/* Location: ./application/models/Judul_model.php */
/* Please DO NOT modify this information : */