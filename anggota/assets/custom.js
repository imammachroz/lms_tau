$(document).ready(function () {
    $("#country").keyup(function () {
        $.ajax({
            type: "POST",
            url: "http://localhost/lms_tau/mahasiswa/GetJudulName",
            data: {
                keyword: $("#nama_prodi").val()
            },
            dataType: "json",
            success: function (data) {
                if (data.length > 0) {
                    $('#DropdownJudul').empty();
                    $('#nama_prodi').attr("data-toggle", "dropdown");
                    $('#DropdownJudul').dropdown('toggle');
                }
                else if (data.length == 0) {
                    $('#country').attr("data-toggle", "");
                }
                $.each(data, function (key,value) {
                    if (data.length >= 0)
                        $('#DropdownJudul').append('<li role="displayCountries" ><a role="menuitem dropdownJudulli" class="dropdownlivalue">' + value['name'] + '</a></li>');
                });
            }
        });
    });
    $('ul.txtnama_judul').on('click', 'li a', function () {
        $('#nama_judul').val($(this).text());
    });
});