<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active"> Buku Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form KRS-->
			<center>
			<div class="col-md-0 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
				<legend><strong>Detail Buku <?php echo $judul_buku; ?></strong></legend>
				<table>
				<tr>
						<strong>ISBN </strong>  &nbsp; <?php echo $isbn; ?>
					<tr>
						<br>
					<tr>
						<strong>Judul Buku </strong> &nbsp; <?php echo $judul_buku;?> 
					</tr>
					<br>
					<tr>
						<strong>Penulis</strong>  &nbsp; <?php echo $penulis;?> 
					</tr>
					<br>
					<tr>
						<strong>Penerbit </strong>  &nbsp; <?php echo $penerbit;?> 
					</tr>
					<br>
					<tr>
						<strong>Kelas </strong>  &nbsp; <?php echo $kelas;?> 
					</tr>
					<br>
					<tr>
						<strong>Kategori </strong>  &nbsp; <?php echo $kategori;?> 
					</tr>
				</table>
			</center>
			<br />
			<table class="table table-bordered table table-striped" style="margin-bottom: 10px;">
				<thead>
				<tr>
						<th scope="col">No</th>	
						<th scope="col">Kode Buku</th>
						<th scope="col">Status</th>
						<th scope="col">Action</th>
					</tr>
</thead>
				<?php
				  $no=1; // Nomor urut dalam menampilkan data
				  //$jumlahSks=0; // Jumlah SKS dimulai dari 0
				  
				  // Menampilkan data KRS
				  foreach($buku_data as $buku)
				  {
				?>
				<tr>
					 <td data-header="No" width="80px"><?php echo $no++; ?></td>
					 <td data-header="Kode Buku"><?php echo $buku->kode_buku;?></td>
					 <td data-header="Status"><?php echo $buku->is_ada;?></td>
					 <td data-header="Action" style="text-align:center" width="120px">
						<?php 	
							// Button untuk melakukan edit KRS
							echo anchor(site_url('judul/update_buku/'.$buku->id_buku),
								 '<button type="button" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></button>'); 
							echo '&nbsp';
							// Button untuk melakukan delete KRS
							echo anchor(site_url('judul/delete_buku/'.$buku->id_buku),
								 '<button type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>',
								 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
						?>
					</td>
				</tr>
				<?php
					}
				?>
			  </table>    
			 <?php 
				// Button untuk melakukan create KRS
				echo anchor(site_url('judul/create_buku/'.$id_judul),'Create', 'class="btn btn-primary"'); 
			 ?>
			 	<a href="<?php echo site_url('/judul') ?>" class="btn btn-default">Cancel</a>
			</div>
