	</div>
		  </div>
		  <!-- /.box -->
		</section>
		<!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->

	  <footer class="main-footer">
		<div class="pull-right hidden-xs">
		  
		</div>
		<strong><a href="https://tau.ac.id">Library Management System</a>.</strong> 2018
	  </footer>  
	</div>
	<!-- ./wrapper -->
	<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
	<!-- SlimScroll -->
	<script src="<?php echo base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
	<!-- FastClick -->
	<script src="<?php echo base_url('assets/bower_components/fastclick/lib/fastclick.js') ?>"></script>
	<!-- AdminLTE App -->
	<script src="<?php echo base_url('assets/js/adminlte.min.js') ?>"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?php echo base_url('assets/js/demo.js') ?>"></script>
	<script>
	  $(document).ready(function () {
		$('.sidebar-menu').tree()
	  })
		$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
				}
				$(this).val(val); 
		});

		//js untuk checkbox show password
		// function myFunction() {
		// 	var x = document.getElementById("password");
		// 	if (x.type === "password") {
		// 		x.type = "text";
		// 	} else {
		// 		x.type = "password";
		// 	}
		// }

		$(".reveal").on('click',function() {
			var $pwd = $(".pwd");
			if ($pwd.attr('type') === 'password') {
					$pwd.attr('type', 'text');
			} else {
					$pwd.attr('type', 'password');
			}
		});
		$(".reveal_email").on('click',function() {
			var $pwd_email = $("#email_password");
			if ($pwd_email.attr('type') === 'password') {
					$pwd_email.attr('type', 'text');
			} else {
					$pwd_email.attr('type', 'password');
			}
		});
	</script>
	</body>
</html>