<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Hari Libur</a></li>
        <li class="active"><?php echo $button ?> Hari Libur</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input atau edit Prodi-->
			<legend><?php echo $button ?> Hari Libur</legend>	
			<form action="<?php echo $action; ?>" method="post">
				<div class="form-group">
					<label for="varchar">Deskripsi <?php echo form_error('nama') ?></label>
					<input type="text" class="form-control" name="nama" id="nama" placeholder="Deskripsi" value="<?php echo $nama; ?>" />
				</div>
				<div class="form-group">
					<label for="varchar">Tanggal <?php echo form_error('tanggal') ?></label>
					<input type="date" class="form-control" name="tanggal" value="<?php echo isset($tanggal) ? set_value('tanggal', date('Y-m-d')) : set_value('tanggal'); ?>" >
				</div>
				
				<input type="hidden" name="id_libur" value="<?php echo $id_libur; ?>" /> 
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
				<a href="<?php echo site_url('libur') ?>" class="btn btn-default">Cancel</a>
			</form>
			<!--// Form Prodi-->