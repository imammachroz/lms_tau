<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Libur extends CI_Controller
{
     // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Libur_model'); // Memanggil Libur_model yang terdapat pada models
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library 
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper		
		$this->load->helper('my_function'); // Memanggil fungsi my_function yang terdapat pada helper		
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman libur
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('libur/libur_list'); // Menampilkan halaman utama libur
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Libur_model->json();
    }
	
    public function create() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
        $data = array(
			'back'   => site_url('libur'),
            'button' => 'Create',
            'action' => site_url('libur/create_action'),
			'id_libur' => set_value('id_libur'),
			'nama' => set_value('nama'),		
			'tanggal' => set_value('tanggal'),		
		);
		
		$this->load->view('header',$dataAdm ); // Menampilkan bagian header dan object data users 	 
        $this->load->view('libur/libur_form', $data); // Menampilkan halaman form libur
		$this->load->view('footer'); // Menampilkan bagian footer
    }
    
    public function create_action() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } 
		else {
            $data = array(
				'id_libur'   => $this->input->post('id_libur',TRUE),
				'nama' => $this->input->post('nama',TRUE),
				'tanggal' => $this->input->post('tanggal',TRUE),
				);           
            $this->Libur_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('libur'));
        }
		
    }
    
    public function update($id) 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
        $row = $this->Libur_model->get_by_id($id);

        if ($row) {
            $data = array(
				'back'   => site_url('libur'),
                'button' => 'Update',
                'action' => site_url('libur/update_action'),
				'id_libur' => set_value('id_libur', $row->id_libur),
				'nama' => set_value('nama', $row->nama),
				'tanggal' => set_value('tanggal', $row->tanggal),
				);
							
			$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 	
            $this->load->view('libur/libur_form', $data); // Menampilkan form mahasiswa
			$this->load->view('footer'); // Menampilkan bagian footer
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('libur'));
        }
    }
    
    public function update_action() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_libur', TRUE));
        } else {
            $data = array(
						'id_libur'   => $this->input->post('id_libur',TRUE),
						'nama' => $this->input->post('nama',TRUE),
						'tanggal' => $this->input->post('tanggal',TRUE),
						);
			
            $this->Libur_model->update($this->input->post('id_libur', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('libur'));
        }
    }
    
    public function delete($id) 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $row = $this->Libur_model->get_by_id($id);

        if ($row) {
            $this->Libur_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('libur'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('libur'));
        }
    }

    public function _rules(){
	$this->form_validation->set_rules('nama', 'deskripsi libur', 'trim|required');
	$this->form_validation->set_rules('tanggal', 'tanggal', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
?>