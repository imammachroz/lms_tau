<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buku_model extends CI_Model
 {

	public function search($q)
	{
		$this->qq = explode(' ',$q);
	}
	public function kategori($q){
		if($q=="a.id_buku"){
			$concat = " ".$q."=".((int)addslashes(trim(implode(" ",$this->qq))));
		} else {
		$concat = "";
		foreach($this->qq as $zx){
			$concat.=' '.($q).' LIKE \'%'.addslashes(strtolower(trim($zx))).'%\' OR';
		}}
		
		$this->wheree = ' WHERE'.rtrim($concat,'OR');
	}
	//query pengambilan semua data
	public function getAllData1()
	{
		return $this->db->query('SELECT a.id_buku,a.ISBN,a.judul,b.kategori,c.nama_penerbit,d.nama_pengarang,e.nama_rak,a.thn_terbit,a.stok,a.ket FROM tb_buku AS a INNER JOIN tb_kategori AS b ON a.id_kategori=b.id_kategori INNER JOIN tb_penerbit AS c ON a.id_penerbit=c.id_penerbit INNER JOIN tb_pengarang AS d ON a.id_pengarang=d.id_pengarang INNER JOIN tb_rak AS e ON a.no_rak=e.no_rak'.(isset($this->wheree)?$this->wheree:''));
	}
	//query pengambilan semua data
	
    
    function getAllData($cat=NULL) {
            
        $this->db->select('judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, 
        kategori.nama_kategori ,penerbit.nama_penerbit as penerbit, penulis.nama_penulis as penulis, COUNT(buku.id_buku) as stok, 
        (CASE WHEN stok_tersedia.stok_tersedia is null then 0 ELSE stok_tersedia.stok_tersedia END) 
        stok_tersedia, (CASE WHEN stok_dipinjam.stok_dipinjam is null then 0 
        ELSE stok_dipinjam.stok_dipinjam END) stok_dipinjam');
        $this->db->from('buku');
        $this->db->join('judul','judul.id_judul=buku.id_judul');
        $this->db->join('kelas','judul.id_kelas=kelas.id_kelas');
		$this->db->join('kategori','kategori.id_kategori=judul.id_kategori');
        $this->db->join('penerbit','penerbit.id_penerbit=judul.id_penerbit');
        $this->db->join('penulis','penulis.id_penulis=judul.id_penulis');
        $this->db->join('(
            SELECT judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, kategori.nama_kategori,
            penerbit.nama_penerbit , penulis.nama_penulis, COUNT(buku.id_buku) as stok_tersedia
            from buku
            LEFT JOIN judul on buku.id_judul=judul.id_judul 
            LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
            LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
            LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
            LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
            WHERE buku.is_ada="Tersedia"
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
			penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
			) stok_tersedia','stok_tersedia.id_judul=buku.id_judul','LEFT',NULL);
        $this->db->join('(
            SELECT judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, kategori.nama_kategori,
            penerbit.nama_penerbit , penulis.nama_penulis, COUNT(buku.id_buku) as stok_dipinjam
            from buku
            LEFT JOIN judul on buku.id_judul=judul.id_judul 
            LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
            LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
            LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
            LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
            WHERE buku.is_ada="Tidak"
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
			penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
			) stok_dipinjam','stok_dipinjam.id_judul=buku.id_judul', 'LEFT',NULL);
        if(!empty($cat)) $this->db->where('judul.id_kategori', $cat);
        $this->db->group_by('judul.id_judul, judul.isbn, judul.judul_buku, 
        penulis.nama_penulis, penerbit.nama_penerbit, kelas.nama_kelas, kategori.nama_kategori');
        $data = $this->db->get();
        return $data;
    }

    public function getAllCategory()
    {
        $query = $this->db->query('SELECT id_kategori,nama_kategori FROM kategori');
        return $query->result_array();
    }

	//menghapus data dalam tabel
	function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}
	function deletedetData($table,$col,$id_det_buku)
	{
		$this->db->where($col,$id_det_buku);
		$aks=$this->db->delete($table);
		return $aks;
	}

	//memasukan data - insert
	function insertData($table,$data)
	{
		$this->db->insert($table,$data);
	}

	//query untuk mengambil detail by id
	function get_detail($table,$id_table,$id) {
		$query = $this->db->get_where($table, array($id_table => $id));
		return $query;
	}
	function get_detail12($table,$col1,$col2,$id,$tgl) {
		$query = $this->db->get_where($table, array($col1 => $id,
						$col2=>$tgl));
		return $query;
	}
	function get_detail1($table,$id_table,$id) {
		$this->db->where($id_table,$id);
		$query = $this->db->get($table);
		$isi=$query->row_array();
		return $isi;
	}
	function get_detail123($table,$id_table,$id) {
		$this->db->where($id_table,$id);
		$p=$this->db->get($table);
		return $p;
	}

	function updateData1($table,$data,$field,$id)
	{
		$this->db->where($field,$id);
		$this->db->update($table,$data);
	}	

	function updateData($table,$data,$field,$key)
	{
		$this->db->where($key,$field);
		$this->db->update($table,$data);
	}	


	public function get_stok($id_buku){
		$query =$this->db->where('id_buku', $id_buku)
						->limit(1)
						->get('tb_buku');
      	if ($query->num_rows() > 0){
			return $query->row();
		} else {
			return array();
		
		}
	}
	function Delete($table,$field,$id)
	{
		$this->db->where($field,$id);
		$this->db->delete($table);
	}	

	//*last edited 10 April 2017
	//batch insert
	function insertData_batch($table,$data)
	{
		$this->db->insert_batch($table,$data);
	}

	public function countRow($status,$id_buku){
		$query = $this->db->query("SELECT status FROM tb_detail_buku WHERE status='".$status."' AND id_buku='".$id_buku."'");
		echo $query->num_rows();
	}

	public function countRow_pinjam($status,$id_pinjam){
		$query = $this->db->query("SELECT status FROM tb_detail_pinjam WHERE status='".$status."' AND id_pinjam='".$id_pinjam."'");
		$query->num_rows();
	}

	public function update_status($table,$data,$field1,$key1, $field2, $key2)
	{
		$this->db->where($field1,$key1);
		$this->db->where($field2,$key2);
		$this->db->update($table,$data);
	}	

	public function update_status2($no_buku, $id_buku, $data)
	{
		$this->db->where('no_buku',$no_buku);
		$this->db->where('id_buku',$id_buku);
		$this->db->update('tb_detail_buku',$data);
	}	

	function get_detail2($table,$id_table, $id) {
		$this->db->where($id_table,$id);
		$this->db->where('status','1');
		$query = $this->db->get($table);
		return $query;
	}

	public function get_total($id_pinjam){
		$query =$this->db->where('id_pinjam', $id_pinjam)
						->limit(1)
						->get('tb_pinjam');
      	if ($query->num_rows() > 0){
			return $query->row();
		} else {
			return array();
		
		}
	}
	/*update tgl 5/5 2017 */
	/*keperluan chart */
	public function get_jml_peminjaman($first_date, $second_date){
		$this->db->select('id_pinjam, COUNT(id_pinjam) as total');
		$this->db->where('tgl_pinjam >=', $first_date);
		$this->db->where('tgl_pinjam <=', $second_date);
		$query = $this->db->get('tb_pinjam');
		if ($query->num_rows() > 0){
			return $query->row();
		} else {
			return array();
		
		}
	}

	public function buku_pinjam(){
		$query = $this->db->query("SELECT tb_buku.id_buku, tb_buku.judul, count(tb_detail_pinjam.id_buku) AS total FROM `tb_buku` JOIN tb_detail_pinjam on tb_buku.id_buku=tb_detail_pinjam.id_buku GROUP BY tb_detail_pinjam.id_buku ORDER BY total DESC LIMIT 10");
		return $query->result_array();
	}


	public function kategori_pinjam(){
		$query = $this->db->query("SELECT tb_kategori.id_kategori, tb_kategori.kategori, count(tb_detail_pinjam.id_buku) AS total FROM `tb_kategori` JOIN tb_buku on tb_buku.id_kategori=tb_kategori.id_kategori JOIN tb_detail_pinjam on tb_detail_pinjam.id_buku=tb_buku.id_buku GROUP BY tb_kategori.id_kategori ORDER BY total DESC LIMIT 10");
		return $query->result_array();
	}

	public function kelas_pinjam(){
		$query = $this->db->query("SELECT tb_kelas.id_kelas, tb_kelas.kelas, count(tb_pinjam.id_anggota) AS total FROM `tb_kelas` JOIN tb_anggota on tb_kelas.id_kelas=tb_anggota.id_kelas JOIN tb_pinjam on tb_pinjam.id_anggota=tb_anggota.id_anggota GROUP BY tb_kelas.id_kelas ORDER BY total DESC LIMIT 10");
		return $query->result_array();
	}
	
}

/* End of file Perpus_model.php */
/* Location: ./application/models/Perpus_model.php */