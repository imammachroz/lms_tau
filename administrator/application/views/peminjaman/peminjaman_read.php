<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li><a href="<?=base_url()?>peminjaman">Peminjaman</a></li>        
        <li class="active"> Detail Peminjaman</li>
      </ol>
		</section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
	
			<!-- Form KRS-->
			<center>
						
		<div class="col-md-0 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
				<legend><strong>DETAIL PEMINJAMAN</strong>
				</legend>
				<table>
					<tr>
						<td1><strong>NIM </strong></td1> <td1> &nbsp; <?php echo $nim; ?></td1>
					<tr>
						<br/>
					<tr>
						<td1><strong>Nama Mahasiswa </strong></td1> <td1> &nbsp; <?php echo $nama; ?></td1>
					<tr>
						<br/>
					<tr>
						<td1><strong>Email </strong></td1> <td1> &nbsp; <?php echo $email_anggota; ?></td1>
					<tr>
				</table>
			</center>
			<br />
			<div style="overflow-x:auto;">
			<table class="table table-bordered table-striped" style="margin-bottom: 10px;">
				<thead>
					<tr >
						<th scope="col" >No</th>	
						<th scope="col" >Kode Buku</th>
						<th scope="col"  >Judul Buku</th>
						<th scope="col" >Tanggal Pinjam</th>
						<th scope="col" >Tanggal Kembali</th>
						<th scope="col" >Denda</th>
						<th scope="col" >ACTION</th>
					</tr>
					</thead>	
				<?php
				  $no=1; // Nomor urut dalam menampilkan data
				  //$jumlahSks=0; // Jumlah SKS dimulai dari 0
				  if (is_array($peminjaman_data) || is_object($peminjaman_data))
				  {
							// Menampilkan data KRS
							foreach($peminjaman_data as $peminjaman)
							{
							?>
							<tr>
								<td data-header="NO" class="title" width="80px"><?php echo $no++; ?></td>
								<td data-header="Kode Buku"><?php echo $peminjaman->kode_buku;?></td>
								<td data-header="Judul Buku"><?php echo $peminjaman->judul_buku;?></td>
								<td data-header="Tanggal Pinjam"><?php echo $peminjaman->tanggal_pinjam;?></td>
								<td data-header="Tanggal Kembali"><?php echo $peminjaman->tanggal_kembali;?></td>
								<td data-header="Denda">Rp. <?php echo $denda=$peminjaman->denda;?></td>
								<td data-header="ACTION" style="text-align:center" width="180px">
									<?php 	
										// Button untuk melakukan edit KRS
										echo anchor(site_url('peminjaman/update_peminjaman/'.$peminjaman->id_peminjaman),
											'<button type="button" class="btn btn-info"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>'); 
										echo ' ';
										// Button untuk melakukan delete KRS
										echo anchor(site_url('peminjaman/pengembalian_buku/'.$peminjaman->id_peminjaman.'/'.$denda),
											'<button type="button" class="btn btn-success"><i class="fa fa-reply" aria-hidden="true"></i></button>',
											'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
											echo ' ';
										echo anchor(site_url('peminjaman/email_peminjam/'.$peminjaman->id_peminjaman),
											'<button type="button" class="btn btn-warning"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>',
											'onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
									?>
								</td>
							</tr>
							<?php
								}
							}
						?>
						
				</table>   
				
				</div> 
			 <?php 
				// Button untuk melakukan create KRS
				echo anchor(site_url('peminjaman/create_peminjaman/'.$id_mahasiswa),'Create', 'class="btn btn-primary"');
			 ?>
				<a href="<?php echo site_url('peminjaman') ?>" class="btn btn-default">Cancel</a>
			</div>

				</section>
				