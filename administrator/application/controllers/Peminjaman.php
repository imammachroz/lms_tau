<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Peminjaman
class Peminjaman extends CI_Controller
{
  // Fungsi untuk menampilkan halaman peminjaman 
   // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Peminjaman_model'); // Memanggil Peminjaman_model yang terdapat pada models
		$this->load->model('Users_model');
		$this->load->model('Anggota_model');
		$this->load->model('Prodi_model');
		$this->load->model('Buku_model');
		$this->load->model('Pengembalian_model');
		$this->load->model('Konfigurasi_peminjaman_model');
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library        
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman peminjaman
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);  		
		$this->load->view('header_list',$dataAdm); // Menampilkan bagian header dan object data peminjaman
        $this->load->view('peminjaman/peminjaman_list'); // Menampilkan halaman peminjaman
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 
    
	// Fungsi JSON
    public function json() {
        header('Content-Type: application/json');
		echo $this->Peminjaman_model->json1();
		//echo $this->Anggota_model->json();
	}
	public function json_peminjaman_mahasiswa() {
        header('Content-Type: application/json');
		//echo $this->Peminjaman_model->json();
		echo $this->Anggota_model->json_peminjaman_mahasiswa();
	}
	
    public function baca_peminjaman($id)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login			
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }
	   /* $this->db->select('peminjaman.id_peminjaman,
        mahasiswa.id_mahasiswa,mahasiswa.nim, mahasiswa.nama_lengkap,
        buku.kode_buku,peminjaman.tanggal_pinjam,
		peminjaman.tanggal_kembali');   */
	// 	$this->db->select('pm.id_peminjaman,pm.id_mahasiswa,pm.id_buku,bk.kode_buku,jdl.judul_buku,
	// 	pm.tanggal_pinjam,pm.tanggal_kembali'); 
	// 	$this->db->from('peminjaman as pm'); 
	// 	//$where_clause="is_kembali='T' and id_mahasiswa='$id'";
    //    //$this->db->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
    //    $this->db->join('buku as bk','bk.id_buku=pm.id_buku');
	//    $this->db->join('judul as jdl','jdl.id_judul=bk.id_judul');
	//    $this->db->where('is_kembali','T');
	//    $this->db->where('id_mahasiswa',$id);
	// , 
		$this->db->select('mahasiswa.id_mahasiswa, peminjaman.id_peminjaman, peminjaman.id_buku,
		buku.kode_buku,judul.judul_buku,peminjaman.tanggal_pinjam
		 ,peminjaman.tanggal_kembali, peminjaman.is_kembali
		 , case when datediff(CURDATE(),peminjaman.tanggal_kembali) < 0 
		 then 0 else datediff(CURDATE(),peminjaman.tanggal_kembali) * konfigurasi_peminjaman.denda end as denda');
		$this->db->from('mahasiswa');
		$this->db->join('peminjaman','mahasiswa.id_mahasiswa=peminjaman.id_mahasiswa');
		$this->db->join('konfigurasi_peminjaman','mahasiswa.id_peminjam=konfigurasi_peminjaman.id_peminjam');
		$this->db->join('buku','buku.id_buku=peminjaman.id_buku');
		$this->db->join('judul','judul.id_judul=buku.id_judul');
		$this->db->where('is_kembali','T');
		$this->db->where('mahasiswa.id_mahasiswa',$id);
		  $peminjaman = $this->db->get()->result();
		// $peminjaman = $this->Peminjaman_model->get_detail_peminjaman($id);
		return $peminjaman;
	}

	public function json_detail($id) {
        header('Content-Type: application/json');
        echo $this->Peminjaman_model->json_detail($id);
    }
	
	// Fungsi untuk menampilkan halaman peminjaman secara detail
    public function read($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampilkan data peminjaman yang ada di database berdasarkan id-nya yaitu id_peminjaman
        $row = $this->Peminjaman_model->get_by_id($id);
		 $id_mahasiswa=$this->input->post('id_mahasiswa',TRUE);
		 $id_peminjaman=$this->input->post('id_peminjaman',TRUE);
         // Menampung data yang diinputkan 	
         $data = array('action' => site_url('peminjaman/peminjaman_action'),
              'id_mahasiswa'=>$id_mahasiswa,
            );
        
		// Jika data peminjaman tersedia maka akan ditampilkan
        if ($row) {
            $dataPeminjam=array(
                'button' => 'Create',
                'back'   => site_url('peminjaman'),
				'peminjaman_data'=>$this->baca_peminjaman($id),
				'id_mahasiswa'=>$this->Anggota_model->get_by_id($id)->id_mahasiswa,
				'nim'=>$this->Anggota_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id)->id_mahasiswa)->nim,
				'nama'=>$this->Anggota_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id)->id_mahasiswa)->nama_lengkap,
				'email_anggota'=>$this->Anggota_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id)->id_mahasiswa)->email,
                );
            $this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
			$this->load->view('peminjaman/peminjaman_read', $dataPeminjam); // Menampilkan halaman detail peminjaman
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika data peminjaman tidak tersedia maka akan ditampilkan informasi 'Record Not Found'
		else {
			$this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
            $this->session->set_flashdata('message', 'Record Not Found');
			$this->load->view('footer'); // Menampilkan bagian footer
            redirect(site_url('peminjaman'));
        }
    }
	
	public function create(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		); 
		 		
		$this->load->view('header_list',$dataAdm); // Menampilkan bagian header dan object data peminjaman
        $this->load->view('peminjaman/mahasiswa_list'); // Menampilkan halaman peminjaman
		$this->load->view('footer_list'); // Menampilkan bagian footer
	}
	

	public function create_peminjaman($id) 
	{
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampung data yang diinputkan 
		$data = array(
			'button' => 'Create',
			'judul'=>'Tambah',
			'back'   => site_url('peminjaman'),
			'action' => site_url('peminjaman/create_peminjaman_action'),
			'id_mahasiswa' => $id,
			'id_buku' => set_value('id_buku'),
			'id_peminjaman' => set_value('id_peminjaman'),
			'is_kembali' => set_value('is_kembali'),
			'tanggal_pinjam' => set_value('tanggal_pinjam'),
			'tanggal_kembali' => set_value('tanggal_kembali'),
	);
	$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
	$this->load->view('peminjaman/peminjaman_form', $data); // Menampilkan form KRS
	$this->load->view('footer'); // Menampilkan bagian footer
	}

	public function search_book(){

		$buku = $this->input->get('buku');
		$query = $this->Peminjaman_model->get_buku($buku,'kode_buku','judul_buku');
		echo json_encode($query);

	}

	public function create_peminjaman_action() 
	{
    // Jika session data username tidak ada maka akan dialihkan kehalaman login			
    if (!isset($this->session->userdata['username'])) {
        redirect(base_url("login"));
    }
    
     $this->_rules_peminjaman(); // Rules atau aturan bahwa setiap form harus diisi
    
    // // Jika form KRS belum diisi dengan benar 
    // // maka sistem akan meminta user untuk menginput ulang
     if ($this->form_validation->run() == FALSE) {
         $this->create_peminjaman($id);
          // Menampilkan data berdasarkan id-nya yaitu username
         
    } 
    // Jika form KRS telah diisi dengan benar 
    // maka sistem akan menyimpan kedalam database
    else {	
        $id_mahasiswa = $this->input->post('id_mahasiswa',TRUE);
        $id_buku = $this->input->post('id_buku',TRUE);

        //$dataPeminjam = $this->Judul_model->get_by_id($id_judul);
        $rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
        $dataAdm = array(	
            'wa'       => 'Web administrator',
            'univ'     => 'Library Management System',
            'username' => $rowAdm->username,
            'email'    => $rowAdm->email,
            'level'    => $rowAdm->level,
        );
		  // Menampilkan data KRS
		  $id_mahasiswa = $this->Peminjaman_model->get_by_id($id_mahasiswa)->id_mahasiswa;
		  $lama_hari=$this->Konfigurasi_peminjaman_model->lama_hari($id_mahasiswa)->jumlah_hari;
		  $date_now = date('Y-m-d');
		  $date_peminjaman = adddate($lama_hari);
          $data = array(
            'id_mahasiswa' => $id_mahasiswa,
			'id_buku' => $id_buku,
			'tanggal_pinjam' => $date_now,
			'tanggal_kembali' => $date_peminjaman,
			);	 
		
		$this->Peminjaman_model->insert($data);

		$this->Buku_model->update_buku_pinjam($id_buku);

		$this->Anggota_model->update_mahasiswa_pinjam($id_mahasiswa);

		$this->session->set_flashdata('message', 'Create Record Success');
        
         //   redirect(site_url('/judul'));
            // Menampilkan data KRS 
			$dataPeminjam=array(
                'button' => 'Create',
                'back'   => site_url('peminjaman'),
				'peminjaman_data'=>$this->baca_peminjaman($id_mahasiswa),
				'id_mahasiswa'=>$this->Anggota_model->get_by_id($id_mahasiswa)->id_mahasiswa,
				'nim'=>$this->Anggota_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id_mahasiswa)->id_mahasiswa)->nim,
				'nama'=>$this->Anggota_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id_mahasiswa)->id_mahasiswa)->nama_lengkap,
                );
			redirect(site_url('peminjaman/read/'.$id_mahasiswa));
            
      
      $this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
      $this->load->view('peminjaman/peminjaman_read',$dataPeminjam); // Menampilkan data KRS
      $this->load->view('footer'); 
    	}
	}

	public function _rules_peminjaman() 
    {
		$this->form_validation->set_rules('id_buku', 'id_buku', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }


	public function pengembalian_buku($id, $denda){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		$id_buku = $this->Pengembalian_model->get_by_id($id)->id_buku;
		$id_mahasiswa=$this->Pengembalian_model->get_by_id($id)->id_mahasiswa;
		$row = $this->Pengembalian_model->get_by_id($id);
		$denda=$this->uri->segment(4);
		
		//jika id nim yang dipilih tersedia maka akan dihapus
        if ($row) {
			// menghapus data berdasarkan id-nya yaitu nim
			if($this->Pengembalian_model->pengembalian_denda($id,$denda)){
				
				// menampilkan informasi 'Delete Record Success' setelah data judul dihapus 
				$this->session->set_flashdata('message', 'Can not Delete This Record !');
			}
			// jika data tidak ada yang dihapus maka akan menampilkan 'Can not Delete This Record !'
			else{
			
				$this->session->set_flashdata('message', 'Delete Record Success');	
			}

			$this->Buku_model->update_buku_kembali($id_buku);

			$this->Anggota_model->update_mahasiswa_kembali($id_mahasiswa);
            redirect(site_url('peminjaman/read/'.$id_mahasiswa));				
			
        } 
		//jika id nim yang dipilih tidak tersedia maka akan muncul pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peminjaman'));
        }
	}

	public function update_peminjaman($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		$denda = 
		$id_buku = $this->Pengembalian_model->get_by_id($id)->id_buku;
		$id_mahasiswa=$this->Pengembalian_model->get_by_id($id)->id_mahasiswa;
		$row = $this->Pengembalian_model->get_by_id($id);
		$lama_hari=$this->Konfigurasi_peminjaman_model->lama_hari($id_mahasiswa)->jumlah_hari;
		
		//jika id nim yang dipilih tersedia maka akan dihapus
        if ($row) {
			// menghapus data berdasarkan id-nya yaitu nim
			if($this->Pengembalian_model->update_peminjaman($id)){
				
				// menampilkan informasi 'Delete Record Success' setelah data judul dihapus 
				$this->session->set_flashdata('message', 'Tidak bisa Perpanjang Peminjaman');
			}
			// jika data tidak ada yang dihapus maka akan menampilkan 'Can not Delete This Record !'
			else{
			
				$this->session->set_flashdata('message', 'Sukses Perpanjang Peminjaman');	
			}

			$this->Pengembalian_model->update_peminjaman($id);



			redirect(site_url('peminjaman/read/'.$id_mahasiswa));		
			
			
        } 
		//jika id nim yang dipilih tidak tersedia maka akan muncul pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peminjaman'));
        }
	}


	public function email_peminjam($id){

		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$pass_email   = decrypt_pass($rowAdm->email_password);
		$id_mahasiswa=$this->Pengembalian_model->get_by_id($id)->id_mahasiswa;
		$nama=$this->Anggota_model->get_by_id($id_mahasiswa)->nama_panggilan;
		$email=$this->Anggota_model->get_by_id($id_mahasiswa)->email;
		$data_peminjaman=$this->Peminjaman_model->get_by_id_peminjaman($id);
		// Konfigurasi email
        $config = [
			'smtp_timeout'=>'30',
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_user' => "$rowAdm->email",    // Ganti dengan email gmail kamu
			'smtp_pass' => "$pass_email",      // Password gmail kamu
			'smtp_port' => 465,
			'crlf'      => "\r\n",
			'newline'   => "\r\n"
		];

	 // Load library email dan konfigurasinya
	 $this->load->library('email', $config);
	 $this->email->initialize($config);
	 // Email dan nama pengirim
	 $this->email->from('no-replay@tau.ac.id','Library TAU');
	 // Email penerima
	 $this->email->to("$email"); // Ganti dengan email tujuan kamu

	 // Lampiran email, isi dengan url/path file
	 //$this->email->attach('https://img/default.png');

	 // Subject email
	 $this->email->subject('Pemberitahuan Peminjaman Buku Tanri Abeng University');

	 $pesan = '	<html>
				<head>
				<title>Birthday Reminders for August</title>
				</head>
				<body>
					<p>Dear '.$nama.',</p>
					<p>Buku yang kamu pinjamkan berjudul '.$data_peminjaman->judul_buku.' 
					dengan kode buku '.$data_peminjaman->kode_buku.'
					pada tanggal <strong>'.$data_peminjaman->tanggal_pinjam.'</strong>
					sudah memasuki keterlambatan <strong>'.$data_peminjaman->sisa_hari.'</strong> hari 
					dari tanggal kembali <strong>'.$data_peminjaman->tanggal_kembali.'</strong>, 
					kamu akan mendapatkan denda sebesar Rp. <strong> '.$data_peminjaman->denda.'</strong>.</p>
					<p>Segera kembalikan buku yang kamu pinjamkan.</p>
					<p>&nbsp;</p>
					<p>Best Regard,</p>
					<p>&nbsp;</p>
					<p>LMS TAU</p>
					<div>
					<div>TANRI ABENG UNIVERSITY</div>
					<div>Tel: +62 21 5890 8888</div>
					</div>
					<div>Fax: +62 21 5890 8118</div>
					<div><a href="http://www.tauniversity.ac.id/">www.tau.ac.id</a></div>
				</body>
				</html>
				';
	 // Isi email
	 $this->email->message($pesan);
	 // Tampilkan pesan sukses atau error
	 if ($this->email->send()) {
		$this->session->set_flashdata('message', 'Email Terkirim ');
		redirect(site_url('peminjaman/read/'.$id_mahasiswa));
		} else {
		$this->session->set_flashdata('message', 'Email tidak dapat dikirim, Coba lagi');
		redirect(site_url('peminjaman/read/'.$id_mahasiswa));
	 }

	}




}
?>