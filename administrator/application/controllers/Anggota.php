<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Mahasiswa
class Anggota extends CI_Controller
{
     // Konstruktor			
	function __construct()
    {
        parent::__construct();
		$this->load->model('Anggota_model'); // Memanggil Anggota_model yang terdapat pada models
		$this->load->model('Prodi_model');
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper
		$this->load->library('upload'); // Memanggil upload yang terdapat pada helper
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
		$this->load->helper('email');
    }
	
	// Fungsi untuk menampilkan halaman mahasiswa
    public function index(){   
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('anggota/anggota_list'); // Menampilkan halaman utama mahasiswa
		$this->load->view('footer_list'); // Menampilkan bagian footer
    }
	
	// Fungsi JSON
	public function json() {
        header('Content-Type: application/json');
        echo $this->Anggota_model->json();
    }
	

	// Fungsi untuk menampilkan halaman mahasiswa secara detail
    public function read($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampilkan data mahasiswa yang ada di database berdasarkan id-nya yaitu nim
        $row = $this->Anggota_model->get_by_id($id);
		
		// Jika data mahasiswa tersedia maka akan ditampilkan
        if ($row) {
            $data = array(
				'button' => 'Detail',
				'back'   => site_url('anggota'),
				'nim' => $row->nim,
				'id_mahasiswa' => $row->id_mahasiswa,
				'id_peminjam' => $row->id_peminjam,
				'id_telegram' => $row->id_telegram,
				'nama_lengkap' => $row->nama_lengkap,
				'alamat' => $row->alamat,
				'email' => $row->email,
				'telp' => $row->telp,
				'tempat_lahir' => $row->tempat_lahir,
				'tgl_lahir' => $row->tgl_lahir,
				'jenis_kelamin' => $row->jenis_kelamin,
				'photo' => $row->photo,
				'id_fakultas' => $row->id_fakultas,
				'id_prodi' => $row->id_prodi,
				'lastedit_date' =>$row->lastedit_date,
			);
            $this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users
			$this->load->view('anggota/anggota_read', $data); // Menampilkan halaman detail mahasiswa
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika data mahasiswa tidak tersedia maka akan ditampilkan informasi 'Record Not Found'
		else {
			$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users
            $this->session->set_flashdata('message', 'Record Not Found');
			$this->load->view('footer'); // Menampilkan bagian footer
            redirect(site_url('anggota'));
        }
    }
	
	// Fungsi menampilkan form Create Mahasiswa
    public function create(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
	  
	  // Menampung data yang diinputkan
      $data = array(
        'button' => 'Create',
		'back'   => site_url('anggota'),
        'action' => site_url('anggota/create_action'),
		'nim' => set_value('nim'),
		'id_mahasiswa' => set_value('id_mahasiswa'),
		'id_peminjam' => set_value('id_peminjam'),
		'id_telegram' => set_value('id_telegram'),
	    'nama_lengkap' => set_value('nama_lengkap'),
	    'alamat' => set_value('alamat'),
	    'email' => set_value('email'),
	    'telp' => set_value('telp'),
	    'tempat_lahir' => set_value('tempat_lahir'),
	    'tgl_lahir' => set_value('tgl_lahir'),
	    'jenis_kelamin' => set_value('jenis_kelamin'),
		'photo' => set_value('photo'),
		'id_prodi' => set_value('id_prodi'),
		'password' => set_value('password'),
		'lastedit_date' => set_value('lastedit_date'),
		'id_sessions' => set_value('id_sessions'),
	  );
        $this->load->view('header',$dataAdm ); // Menampilkan bagian header dan object data users 	 
        $this->load->view('anggota/anggota_form', $data); // Menampilkan halaman form mahasiswa
		$this->load->view('footer'); // Menampilkan bagian footer
    }
    
	// Fungsi untuk melakukan aksi simpan data
    public function create_action(){
		
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $this->_rules(); // Rules atau aturan bahwa setiap form harus diisi
		
		// Jika form mahasiswa belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } 
		// Jika form mahasiswa telah diisi dengan benar 
		// maka sistem akan menyimpan kedalam database
		else{	
			// Konfigurasi untuk melakukan upload photo
			$image_path = realpath(APPPATH . '../../assets/images/');
			$config['upload_path']   = $image_path;   //path folder
			$config['overwrite'] = TRUE;
			$config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg	
			$config['max_size']	= '4096000';		
			$config['file_name']     = url_title($this->input->post('nim')); //nama file photo dirubah menjadi nama berdasarkan nim	
			$this->upload->initialize($config);
			
			// Jika file photo ada 
			if(!empty($_FILES['photo']['name'])){	
			
				// Menghapus file image lama
				unlink($image_path.$this->input->post('photo'));	
				
				// Upload file image baru
				if ($this->upload->do_upload('photo')){
					$photo = $this->upload->data();	

					$config['image_library']='gd2';
					$config['source_image']='../assets/images/'.$photo['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= TRUE;
					$config['width']= 200;
					$config['height']= 200;
					$config['new_image']= '../assets/images/'.$photo['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$dataphoto = $photo['file_name'];					
					$this->load->library('upload', $config);
					
					// Menampung data yang diinputkan
					$data = array(
						'nim' => $this->input->post('nim',TRUE),
						'id_telegram' => $this->input->post('id_telegram',TRUE),
						'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
						'id_peminjam' => $this->input->post('id_peminjam',TRUE),
						'alamat' => $this->input->post('alamat',TRUE),
						'email' => $this->input->post('email',TRUE),
						'telp' => $this->input->post('telp',TRUE),
						'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
						'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
						'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
						'photo' => $dataphoto, 
						'id_prodi' => $this->input->post('id_prodi',TRUE),
					); 

					$data_user = array(
						'username' => $this->input->post('nim',TRUE),
						'email' => $this->input->post('email',TRUE),
						'password' => encrypt_pass($this->input->post('nim',TRUE)),
						'id_sessions' => encrypt_pass($this->input->post('nim',TRUE)),
					);
	
					$this->Users_model->insert($data_user);
					
					$this->Anggota_model->insert($data);
				}
				
				$this->session->set_flashdata('message', 'Create Record Success');
				redirect(site_url('anggota'));			
			}
			// Jika file photo kosong 
			else{		
				// Menampung data yang diinputkan
				
				$data = array(
					'nim' => $this->input->post('nim',TRUE),
					'id_telegram' => $this->input->post('id_telegram',TRUE),
					'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
					'alamat' => $this->input->post('alamat',TRUE),
					'email' => $this->input->post('email',TRUE),
					'telp' => $this->input->post('telp',TRUE),
					'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
					'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
					'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
					'id_prodi' => $this->input->post('id_prodi',TRUE),
				);
				
				$data_user = array(
					'username' => $this->input->post('nim',TRUE),
					'email' => $this->input->post('email',TRUE),
					'password' => encrypt_pass($this->input->post('nim',TRUE)),
					'id_sessions' => encrypt_pass($this->input->post('nim',TRUE)),
				);

				$this->Users_model->insert($data_user);
				
				$this->Anggota_model->insert($data);

				$this->session->set_flashdata('message', 'Create Record Success');
				redirect(site_url('anggota'));	
			}
			
        }
    }
    
	// Fungsi menampilkan form Update Mahasiswa
    public function update($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		// Menampilkan data berdasarkan id-nya yaitu nim
        $row = $this->Anggota_model->get_by_id($id);
		
		$datenow = date('Y-m-d H:i:s');

		// Jika id-nya dipilih maka data mahasiswa ditampilkan ke form edit mahasiswa
        if ($row) {
            $data = array(
                'button' => 'Update',
				'back'   => site_url('anggota'),
                'action' => site_url('anggota/update_action'),
				'nim' => set_value('nim', $row->nim),
				'id_mahasiswa' => set_value('id_mahasiswa', $row->id_mahasiswa),
				'id_peminjam' => set_value('id_peminjam', $row->id_peminjam),
				'id_telegram' => set_value('id_telegram', $row->id_telegram),
				'nama_lengkap' => set_value('nama_lengkap', $row->nama_lengkap),
				'alamat' => set_value('alamat', $row->alamat),
				'email' => set_value('email', $row->email),
				'telp' => set_value('telp', $row->telp),
				'tempat_lahir' => set_value('tempat_lahir', $row->tempat_lahir),
				'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),
				'jenis_kelamin' => set_value('jenis_kelamin', $row->jenis_kelamin),
				'photo' => set_value('photo', $row->photo),
				'id_prodi' => set_value('id_prodi', $row->id_prodi),
				'lastedit_date' => set_value('lastedit_date', $datenow),
			);
		    $this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 
            $this->load->view('anggota/anggota_form', $data); // Menampilkan form mahasiswa
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika id-nya yang dipilih tidak ada maka akan menampilkan pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('anggota'));
        }
    }
    
	// Fungsi untuk melakukan aksi update data
    public function update_action(){
		
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $this->_rules(); // Rules atau aturan bahwa setiap form harus diisi	 			
		
		// Jika form mahasiswa belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_mahasiswa', TRUE));
        } 
		// Jika form mahasiswa telah diisi dengan benar 
		// maka sistem akan melakukan update data mahasiswa kedalam database
		else{	
			// Konfigurasi untuk melakukan upload photo
			$image_path = realpath(APPPATH . '../../assets/images/');
			$config['upload_path']   = $image_path;   //path folder
			$config['overwrite'] = TRUE;
			$config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diupload jpg|png|jpeg	
			$config['max_size']	= '4096000';		
			$config['file_name']     = url_title($this->input->post('nim')); //nama file photo dirubah menjadi nama berdasarkan nim	
			$this->upload->initialize($config);
			
			// Jika file photo ada 
			if(!empty($_FILES['photo']['name'])){	
			
				// Menghapus file image lama
				unlink($image_path.$this->input->post('photo'));	
				
				// Upload file image baru
				if ($this->upload->do_upload('photo')){
					$photo = $this->upload->data();	

					$config['image_library']='gd2';
					$config['source_image']='../assets/images/'.$photo['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= TRUE;
					$config['width']= 200;
					$config['height']= 200;
					$config['new_image']= '../assets/images/'.$photo['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$dataphoto = $photo['file_name'];					
					$this->load->library('upload', $config);
					
					// Menampung data yang diinputkan
					$data = array(
						'nim' => $this->input->post('nim',TRUE),
						'id_telegram' => $this->input->post('id_telegram',TRUE),
						'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
						'id_peminjam' => $this->input->post('id_peminjam',TRUE),
						'alamat' => $this->input->post('alamat',TRUE),
						'email' => $this->input->post('email',TRUE),
						'telp' => $this->input->post('telp',TRUE),
						'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
						'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
						'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),
						'photo' => $dataphoto, 
						'id_prodi' => $this->input->post('id_prodi',TRUE),
						'lastedit_date' => $this->input->post('lastedit_date',TRUE),
					); 
					$data_user = array(
						'email' => $this->input->post('email',TRUE),
					);
	
					$this->Users_model->update($this->input->post('nim', TRUE), $data_user); 
					
					$this->Anggota_model->update($this->input->post('id_mahasiswa', TRUE), $data);
				}
				
				$this->session->set_flashdata('message', 'Update Record Success');
				redirect(site_url('anggota/read/'.$this->input->post('id_mahasiswa',TRUE)));			
			}
			// Jika file photo kosong 
			else{		
				// Menampung data yang diinputkan
				$data = array(
					'nim' => $this->input->post('nim',TRUE),
					'id_telegram' => $this->input->post('id_telegram',TRUE),
					'id_peminjam' => $this->input->post('id_peminjam',TRUE),
					'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
					'alamat' => $this->input->post('alamat',TRUE),
					'email' => $this->input->post('email',TRUE),
					'telp' => $this->input->post('telp',TRUE),
					'tempat_lahir' => $this->input->post('tempat_lahir',TRUE),
					'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
					'jenis_kelamin' => $this->input->post('jenis_kelamin',TRUE),	
					'id_prodi' => $this->input->post('id_prodi',TRUE),
					'lastedit_date' => $this->input->post('lastedit_date',TRUE),
				);
				$data_user = array(
					'email' => $this->input->post('email',TRUE),
				);

				$this->Users_model->update($this->input->post('nim', TRUE), $data_user);          
				
				$this->Anggota_model->update($this->input->post('id_mahasiswa', TRUE), $data);

				$this->session->set_flashdata('message', 'Update Record Success');
				redirect(site_url('anggota/read/'.$this->input->post('id_mahasiswa',TRUE)));	
			}
			
        }
    }
    
	// Fungsi untuk melakukan aksi delete data berdasarkan id yang dipilih
    public function delete($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $row = $this->Anggota_model->get_by_id($id);
		$nim = $this->Anggota_model->get_by_id($id)->nim;
		//jika id nim yang dipilih tersedia maka akan dihapus
        if ($row) {
			// menghapus data berdasarkan id-nya yaitu nim
			$this->Anggota_model->delete($id);
				
			$this->Users_model->delete($nim);
				// menampilkan informasi 'Delete Record Success' setelah data mahasiswa dihapus 
				$this->session->set_flashdata('message', 'Delete Record Success');
				
				// menghapus file photo
				unlink("../../assets/images/".$row->photo);
			
			// jika data tidak ada yang dihapus maka akan menampilkan 'Can not Delete This Record !'
		
            redirect(site_url('anggota'));				
			
        } 
		//jika id nim yang dipilih tidak tersedia maka akan muncul pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('anggota'));
        }
    }
	
	// Fungsi rules atau aturan untuk pengisian pada form (create/input dan update)
    public function _rules() 
    {
	$this->form_validation->set_rules('nim', 'nim', 'trim|required');
	$this->form_validation->set_rules('id_peminjam', 'id peminjam', 'trim|required|is_natural_no_zero');
	$this->form_validation->set_rules('nama_lengkap', 'nama lengkap', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim');
	$this->form_validation->set_rules('email', 'email', 'trim|required|valid_emails|xss_clean');
	$this->form_validation->set_rules('telp', 'telp', 'trim');
	$this->form_validation->set_rules('tempat_lahir', 'tempat lahir', 'trim|required');
	$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
	$this->form_validation->set_rules('id_prodi', 'id prodi', 'trim|required|is_natural_no_zero');
	$this->form_validation->set_rules('jenis_kelamin', 'jenis kelamin', 'trim|required');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}
	//cek pipeline

}
?>