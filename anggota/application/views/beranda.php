<?php
	ini_set('display_errors', '0');
    ini_set('error_reporting', E_ALL);
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LMS TAU</title>
  <link rel="icon" href="<?=base_url()?>/favicon.gif" type="image/gif">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min.css')?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/_all-skins.min.css')?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url('assets/img/tau logo.png') ?>" class="logo-image" alt="Logo"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo base_url('assets/img/tau logo.png') ?>" class="logo-image" alt="Logo">
      <b>LMS</b> TAU</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url()?>../assets/images/<?php echo $photo; ?>" onerror="this.src='<?php echo base_url('assets/img/user.png') ?>'" class="user-image">
              <span class="hidden-xs"><?php echo $nama_panggilan; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url()?>../assets/images/<?php echo $photo; ?>" onerror="this.src='<?php echo base_url('assets/img/user.png') ?>'" class="img-circle">

                <p>
                  <?php echo $nama_panggilan; ?>
                  <small><?php echo $univ; ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#"><?php echo $level; ?></a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#"><?php echo $email; ?></a>
                  </div>
                  
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
              <div class="pull-left">
                  <a href="<?=base_url()?>users/update/<?php echo $username ?>" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url()?>admin/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>../assets/images/<?php echo $photo; ?>" onerror="this.src='<?php echo base_url('assets/img/user.png') ?>'" class="img-circle" >
        </div>
        <div class="pull-left info">
          <p><?php echo $nama_panggilan; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>      
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU USERS</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i> <span>LMS MASTER</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('anggota/read/'.$id_mahasiswa) ?>".$id_mahasiswa><i class="fa fa-user"></i> Anggota</a></li>
            <li><a href="<?=base_url()?>judul"><i class="fa fa-book"></i> Buku</a></li>
          </ul>
        </li>
        <li><a href="<?php echo site_url('peminjaman/read/'.$id_mahasiswa) ?>" ><i class="fa fa-shopping-cart"></i> <span>Peminjaman</span></a></li>
        <li><a href="<?=base_url()?>users/update/<?php echo $username ?>"><i class="fa fa-vcard-o"></i> <span>Pengaturan Users</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->
        
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hi 
		<?php
			echo ($nama_panggilan); 
		?>, Selamat datang di LMS Library Management System		        
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Control Panel</li>
      </ol>
    </section>

    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">               
      
          <div class="box box-default color-palette-box">
          <div class="box-body">
				<div class="col-md-12 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
				<div>
			<div class="box-header with-border">
			  <h3 class="box-title"><i class="fa fa-gears"></i> Control Panel</h3>
			</div>
      
			<div class="box-body">
			  <div class="row">
				<div class="col-sm-4 col-md-2">
				  <h4 class="text-center"><span class="info-box-text">Anggota </span></h4>
				 
				  <div class="color-palette-set">
					<a href="<?php echo site_url('anggota/read/'.$id_mahasiswa) ?> "><center><i class="fa fa-user" style="font-size:48px;color:#3c8dbc"></i><center></a>					
				  </div>
				</div>
				<!-- /.col -->
				<div class="col-sm-4 col-md-2">
				  <h4 class="text-center"><span class="info-box-text">Buku</span></h4>
				  <div class="color-palette-set">
					<a href="<?php echo site_url('judul') ?>"><center><i class="fa fa-book" style="font-size:48px;color:#3c8dbc"></i><center></a>
				  </div>
				</div>
				<!-- /.col -->
				<div class="col-sm-4 col-md-2">
				  <h4 class="text-center"><span class="info-box-text">Peminjaman</span></h4>
				  <div class="color-palette-set">
					<a href="<?php echo site_url('peminjaman/read/'.$id_mahasiswa) ?>"><center><i class="fa fa-exchange" style="font-size:48px;color:#3c8dbc"></i><center></a>	
				  </div>
				</div>
				<!-- /.col -->
				<div class="col-sm-4 col-md-2">
				  <h4 class="text-center"><span class="info-box-text">Users</span></h4>
				  <div class="color-palette-set">
					<a href="<?=base_url()?>users/update/<?php echo $username ?>"><center><i class="fa fa-vcard-o" style="font-size:48px;color:#3c8dbc"></i><center></a>	
				  </div>
				</div>
			  </div>
        <!-- /.row -->
        <br/>
			</div>
			<!-- /.box-body -->
		  </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
    <strong><a href="https://tau.ac.id">Library Management System</a>.</strong> 2018
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/bower_components/fastclick/lib/fastclick.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/js/adminlte.min.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/js/demo.js')?>"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
