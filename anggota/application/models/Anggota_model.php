<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Anggota_model extends CI_Model
{
	// Property yang bersifat public   
    public $table = 'mahasiswa';
    public $id = 'id_mahasiswa';
    public $order = 'DESC';
    public $nim = 'nim';
    
	// Konstrutor    
    function __construct()
    {
        parent::__construct();
    }
	
	// Tabel data dengan nama mahasiswa
    function json() {		
        $this->datatables->select("id_mahasiswa,nim, id_telegram, id_peminjam, nama_lengkap, alamat, email, telp, IF(jenis_kelamin = 'P', 'Perempuan', 'Laki-laki') as jenisKelamin");
        $this->datatables->from('mahasiswa');        
        $this->datatables->add_column('action', anchor(site_url('mahasiswa/read/$1'),'<button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button>')."  ".anchor(site_url('mahasiswa/update/$1'),'<button type="button" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></button>')."  ".anchor(site_url('mahasiswa/delete/$1'),'<button type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_mahasiswa');
        return $this->datatables->generate();
    }
    
    function json_peminjaman_mahasiswa() {		
        $this->datatables->select("id_mahasiswa, nim, nama_lengkap, alamat, email, telp, IF(jenis_kelamin = 'P', 'Perempuan', 'Laki-laki') as jenisKelamin");
        $this->datatables->from('mahasiswa');        
        $this->datatables->add_column('action', anchor(site_url('peminjaman/read/$1'),'<button type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></button>')."  ".anchor(site_url('mahasiswa/update/$1'),'<button type="button" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></button>')."  ".anchor(site_url('mahasiswa/delete/$1'),'<button type="button" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_mahasiswa');
        return $this->datatables->generate();
    }
    // Menampilkan semua data 
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // Menampilkan semua data berdasarkan id-nya
    function get_by_id($id)
    {
        $this->db->select('SUBSTRING_INDEX( mahasiswa.nama_lengkap, " ", 1 ) as nama_panggilan, id_mahasiswa,nim, id_peminjam, id_telegram, mahasiswa.id_prodi,prodi.id_fakultas, nama_lengkap, 
        alamat, email, telp, jenis_kelamin, tempat_lahir, tgl_lahir, photo, 
        CONCAT(
            CASE DAYOFWEEK(lastedit_date)
              WHEN 1 THEN "Minggu"
              WHEN 2 THEN "Senin"
              WHEN 3 THEN "Selasa"
              WHEN 4 THEN "Rabu"
              WHEN 5 THEN "Kamis"
              WHEN 6 THEN "Jumat"
              WHEN 7 THEN "Sabtu"
            END,", ",
            DAY(lastedit_date)," ",
            CASE MONTH(lastedit_date) 
              WHEN 1 THEN "Januari" 
              WHEN 2 THEN "Februari" 
              WHEN 3 THEN "Maret" 
              WHEN 4 THEN "April" 
              WHEN 5 THEN "Mei" 
              WHEN 6 THEN "Juni" 
              WHEN 7 THEN "Juli" 
              WHEN 8 THEN "Agustus" 
              WHEN 9 THEN "September"
              WHEN 10 THEN "Oktober" 
              WHEN 11 THEN "November" 
              WHEN 12 THEN "Desember" 
            END," ",
            YEAR(lastedit_date)," ",HOUR(lastedit_date),":", MINUTE(lastedit_date)
          ) AS lastedit_date ');
        $this->db->join('prodi','mahasiswa.id_prodi=prodi.id_prodi'); 
        $this->db->join('fakultas','fakultas.id_fakultas=prodi.id_fakultas'); 
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    function update_mahasiswa_pinjam ($id) {
        $this->db->set('jumlah_buku_pinjam','`jumlah_buku_pinjam` + 1', FALSE);
        $this->db->where($this->id, $id);
        $this->db->update($this->table);
    }

    function update_mahasiswa_kembali ($id) {
        $this->db->set('jumlah_buku_pinjam','`jumlah_buku_pinjam` - 1', FALSE);
        $this->db->where($this->id, $id);
        $this->db->update($this->table);
    }

    // menampilkan jumlah data	
    function total_rows($q = NULL) {
        $this->db->like('nim', $q);
		$this->db->or_like('nim', $q);
		$this->db->or_like('nama_lengkap', $q);
		$this->db->or_like('alamat', $q);
		$this->db->or_like('email', $q);
		$this->db->or_like('telp', $q);
		$this->db->or_like('tempat_lahir', $q);
		$this->db->or_like('tgl_lahir', $q);
		$this->db->or_like('jenis_kelamin', $q);
		$this->db->or_like('id_prodi', $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Menampilkan data dengan jumlah limit
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('nim', $q);
		$this->db->or_like('nim', $q);
		$this->db->or_like('nama_lengkap', $q);
		$this->db->or_like('alamat', $q);
		$this->db->or_like('email', $q);
		$this->db->or_like('telp', $q);
		$this->db->or_like('tempat_lahir', $q);
		$this->db->or_like('tgl_lahir', $q);
		$this->db->or_like('jenis_kelamin', $q);
		$this->db->or_like('id_prodi', $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // Menambahkan data kedalam database
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // Merubah data kedalam database
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function update_by_nim($nim, $data)
    {
        $this->db->where($this->nim, $nim);
        $this->db->update($this->table, $data);
    }

    
    // Menghapus data kedalam database
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Mahasiswa_model.php */
/* Location: ./application/models/Mahasiswa_model.php */
/* Please DO NOT modify this information : */