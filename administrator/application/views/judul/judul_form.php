<style type="text/css">
		input[type=number]::-webkit-outer-spin-button,
input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance:textfield;
}
</style>
<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Buku</a></li>
        <li class="active"><?php echo $button ?> Buku</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input dan edit Judul-->
			<legend><?php echo $button ?> Buku</legend>		 
			<form role="form" class="form-horizontal"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <input type="hidden"  class="form-control" name="cover" id="cover" value="<?php echo $cover; ?>" />
                <input type="hidden" class="form-control" name="id_judul" id="id_judul" value="<?php echo $id_judul; ?>" />	
                
        <div class="form-group"> 
					<label class="col-sm-2" for="int">Kategori </label>
					<div class="col-sm-4">
						 <?php 
							   $query = $this->db->query('SELECT id_kategori,nama_kategori FROM kategori');
							   $dropdowns = $query->result();
							   foreach($dropdowns as $dropdown) {
									   $dropDownList[$dropdown->id_kategori] = $dropdown->nama_kategori;
									}
								  $finalDropDown = array_merge(array("0" => "-- Pilihan --"), $dropDownList); 
							  echo  form_dropdown('id_kategori',$finalDropDown , $id_kategori, 
								    'class="form-control" id="id_kategori"'); 	
							  echo form_error('id_kategori') 
						  ?> 
					</div>
                </div>
                
                <div class="form-group"> 
					<label class="col-sm-2" for="int">Kelas </label>
					<div class="col-sm-4">
						 <?php 
							   $query = $this->db->query('SELECT id_kelas,nama_kelas FROM kelas');
							   $dropdowns = $query->result();
							   foreach($dropdowns as $dropdown) {
									   $dropDownList[$dropdown->id_kelas] = $dropdown->nama_kelas;
									}
								  $finalDropDown = array_merge(array("0" => "-- Pilihan --"), $dropDownList); 
							  echo  form_dropdown('id_kelas',$finalDropDown , $id_kelas, 
								    'class="form-control" id="id_kelas"'); 	
							  echo form_error('id_kelas') 
						  ?> 
					</div>
					
				</div>
                
                <div class="form-group">
					<label class="col-sm-2" for="char">ISBN</label>
					<div class="col-sm-4">
						<input type="number" min="0" class="form-control" name="isbn" id="isbn" placeholder="ISBN" value="<?php echo $isbn; ?>" />
						<?php echo form_error('isbn'); ?>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-2" for="varchar">Judul Buku</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="judul_buku" id="judul_buku" placeholder="Judul Buku" value="<?php echo $judul_buku; ?>" />
						<?php echo form_error('judul_buku') ?>
					</div>
				</div>	
						
				<div class="form-group">
					<label class="col-sm-2" for="varchar">Penulis </label>
					<div class="col-sm-4">	
						<select class="Penulis form-control"  id="id_penulis" name="id_penulis" value="<?php echo $id_penulis; ?>"> </select>
						<?php echo form_error('penulis') ?>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2"  for="varchar">Penerbit </label>
					<div class="col-sm-4">
						<select class="Penerbit form-control" name="id_penerbit" id="id_penerbit" value="<?php echo $id_penerbit; ?>"> </select>
						<?php echo form_error('penerbit') ?>
					</div>
				</div>
					 
				
				
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
					<a href="<?php echo site_url('judul') ?>" class="btn btn-default">Cancel</a>
				</form>  

				<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
				<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
				<script type="text/javascript">
				$('.Penulis').select2({
					placeholder: {
						id:'-1',
						text:'--- Pilih Penulis ---',		
						allowClear: true,				
					},
					//minimumInputLength: 1,
					//allowClear: true,
					width: '100%',
					ajax: {
						url: '<?=base_url()?>/judul/search_penulis',
						dataType: 'json',
						delay: 250,
						data : function(params){
							return{
								penulis:params.term
							};
						},
						processResults: function (data) {
							var results = [];
							
							$.each(data, function(index,item,item1){
								results.push({
									id: item.id_penulis,
									text: item.nama_penulis
								});
							});
							return {
								results: results
							};
						}
					}
				});

				$('.Penerbit').select2({
					placeholder: '--- Pilih Penerbit ---',
					//minimumInputLength: 1,
					//allowClear: true,
					width: '100%',
					ajax: {
						url: '<?=base_url()?>/judul/search_penerbit',
						dataType: 'json',
						delay: 250,
						data : function(params){
							return{
								penerbit:params.term
							};
						},
						processResults: function (data) {
							var results = [];
							
							$.each(data, function(index,item,item1){
								results.push({
									id: item.id_penerbit,
									text: item.nama_penerbit
								});
							});
							return {
								results: results
							};
						}
					}
				});
				</script>