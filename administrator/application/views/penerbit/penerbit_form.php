<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Penerbit</a></li>
        <li class="active"><?php echo $button ?> Penerbit</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
		<!-- Form input dan edit Penerbit-->
		<legend><?php echo $button ?> Penerbit</legend>
        <form action="<?php echo $action; ?>" method="post">
		<input type="hidden" class="form-control" name="id_penerbit" id="id_penerbit" value="<?php echo $id_penerbit; ?>" />
	    <div class="form-group">
            <label for="varchar">Nama Penerbit <?php echo form_error('nama_penerbit') ?></label>
            <input type="text" class="form-control" name="nama_penerbit" id="nama_penerbit" placeholder="Nama Penerbit" value="<?php echo $nama_penerbit; ?>" />
        </div>	
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('penerbit') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>