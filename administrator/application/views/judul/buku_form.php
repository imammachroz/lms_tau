<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('/judul/read/'.$id_judul) ?>"> Data Buku</a></li>
        <li class="active"><?php echo $this->Judul_model->get_by_id($id_judul)->judul_buku ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input dan edit data KRS-->
			<legend>Input buku baru <?php echo $this->Judul_model->get_by_id($id_judul)->judul_buku ;?>  </legend>

			<form action="<?php echo $action; ?>" method="post">
			 <div class="form-group">
				<!-- <label for="int">ID Judul <?php echo form_error('id_judul') ?></label> -->
				<input type="hidden" class="form-control" name="id_judul" value="<?php echo $id_judul; ?>"  />
				<input type="hidden" class="form-control" name="id_buku" id="id_buku" value="<?php echo $id_buku; ?>" />	
			 </div>
			 <div class="form-group">
				<label for="char">Kode Buku <?php echo form_error('kode_buku') ?></label>
				<input type="text" class="form-control" name="kode_buku" id="kode_buku" placeholder="Kode Buku" value="<?php echo $kode_buku; ?>" />
			 </div>
			 <div class="form-group">        
					<label  for="varchar">Status</label>
					<div >
						<?php 
							$pil_status= array(
													"Tersedia" => "Tersedia", 
													"Tidak" => "Tidak");
							echo form_dropdown('is_ada', $pil_status,$is_ada, 'class="form-control" id="is_ada"'); 
							echo form_error('is_ada') 
						?>   
					</div>
				</div>
			 <button type="submit" class="btn btn-primary">Simpan</button> 
					<a href="<?php echo site_url('/judul/read/'.$id_judul) ?>" class="btn btn-default">Cancel</a>
			</form>
   