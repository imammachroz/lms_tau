
<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active"> Buku Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form KRS-->
			<center>
				<legend><strong>Detail Buku <?php echo $judul_buku; ?></strong></legend>
				<table class="styled-table">
					<tr>
						<strong>ISBN </strong> &nbsp; <?php echo $isbn; ?>
					<tr>
						<br>
					<tr>
						<strong>Judul Buku </strong> &nbsp; <?php echo $judul_buku;?>
					</tr>
					<br>
					<tr>
						<strong>Penulis</strong> &nbsp; <?php echo $penulis;?> 
					</tr>
					<br>
					<tr>
						<strong>Penerbit </strong> &nbsp; <?php echo $penerbit;?> 
					</tr>
					<br>
					<tr>
						<strong>Kelas </strong> &nbsp; <?php echo $kelas;?> 
					</tr>
					<br>
					<tr>
						<strong>Kategori </strong> &nbsp; <?php echo $kategori;?> 
					</tr>
				</table>
			</center>
			<br />
			<table class="table table-bordered table-striped" style="margin-bottom: 10px;">
				<thead>
					<tr>
						<th scope="col">NO</th>	
						<th scope="col">Kode Buku</th>
						<th scope="col">Status</th>
					</tr>
				</thead>
				<?php
				  $no=1; // Nomor urut dalam menampilkan data
				  //$jumlahSks=0; // Jumlah SKS dimulai dari 0
				  
				  // Menampilkan data KRS
				  foreach($buku_data as $buku)
				  {
				?>
				<tr>
					 <td data-header="NO" width="80px"><?php echo $no++; ?></td>
					 <td data-header="Kode Buku"><?php echo $buku->kode_buku;?></td>
					 <td data-header="Status"><?php echo $buku->is_ada;?></td>
					</td>
				</tr>
				<?php
					}
				?>
			  </table>    
			 <?php 
				// Button untuk melakukan create KRS
				echo anchor(site_url('judul'),'Back', 'class="btn btn-primary"'); 
			 ?>
			</div>
