<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kelas</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
		<!-- Tampil Data Kategori -->
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <h2 style="margin-top:0px">Kelas</h2>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('kelas/create'), '<i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Create', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>		    
					<th>Nama Kelas</th>	
                    <th>Kode Kelas</th>
					<th width="200px">Action</th>
                </tr>
            </thead>
	    
        </table>
        <script src="assets/js/resp/jquery-3.3.1.js"></script>
        <script src="assets/js/resp/jquery.dataTables.min.js"></script>
        <script src="assets/js/resp/dataTables.rowReorder.min.js"></script>
        <script src="assets/js/resp/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        
        <link rel="stylesheet" type="text/css" href="assets/css/rowReorder.dataTables.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.dataTables.min.css"/>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    dom: '<lf<tr>ipB>',
                    oLanguage: {
                        sProcessing: "<div id='loader'></div>"
                    },
                    bAutoWidth : false, 
                    responsive: true,
                    rowReorder: {
                            selector: 'td:nth-child(0)'
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "kelas/json", "type": "POST"},
                    columns: [
                        {
                            "data": "id_kelas",
                            "orderable": false
                        },						
						{"data": "kode_kelas"},	
						{"data": "nama_kelas"},	
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
		<!--// Tampil Data Kategori -->