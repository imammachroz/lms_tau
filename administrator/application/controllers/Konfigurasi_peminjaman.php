<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Konfigurasi_peminjaman
class Konfigurasi_peminjaman extends CI_Controller
{
    // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Konfigurasi_peminjaman_model'); // Memanggil Konfigurasi_peminjaman_model yang terdapat pada models
		$this->load->model('Users_model');
		$this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library        
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman konfigurasi_peminjaman
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);  		
		$this->load->view('header_list',$dataAdm); // Menampilkan bagian header dan object data konfigurasi_peminjaman
        $this->load->view('konfigurasi_peminjaman/konfigurasi_peminjaman_list'); // Menampilkan halaman konfigurasi_peminjaman
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 
    
	// Fungsi JSON
    public function json() {
        header('Content-Type: application/json');
        echo $this->Konfigurasi_peminjaman_model->json();
    }
    
	// Fungsi menampilkan form Create Konfigurasi_peminjaman
    public function create(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);  		
		
		// Menampung data yang diinputkan
        $data = array(
            'button' => 'Create',
			'back'   => site_url('konfigurasi_peminjaman'),
            'action' => site_url('konfigurasi_peminjaman/create_action'),
	        'id_peminjam' => set_value('id_peminjam'),
	        'deskripsi_peminjam' => set_value('deskripsi_peminjam'),
	        'jumlah_hari' => set_value('jumlah_hari'),	     
	        'denda' => set_value('denda'),	     
		);
		
		$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data konfigurasi_peminjaman
        $this->load->view('konfigurasi_peminjaman/konfigurasi_peminjaman_form', $data); // Menampilkan halaman utama yaitu form konfigurasi_peminjaman 
		$this->load->view('footer'); // Menampilkan bagian footer
    }
    
	// Fungsi untuk melakukan aksi simpan data
    public function create_action(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $this->_rules(); // Rules atau aturan bahwa setiap form harus diisi
		
		// Jika form konfigurasi_peminjaman belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } 
		// Jika form konfigurasi_peminjaman telah diisi dengan benar 
		// maka sistem akan menyimpan kedalam database
		else {
            $data = array(
				'id_peminjam' => $this->input->post('id_peminjam',TRUE),
				'deskripsi_peminjam' => $this->input->post('deskripsi_peminjam',TRUE),
				'jumlah_hari' => $this->input->post('jumlah_hari',TRUE),
				'denda' => $this->input->post('denda',TRUE),
	    );
           
            $this->Konfigurasi_peminjaman_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('konfigurasi_peminjaman'));
        }
    }
    
	// Fungsi menampilkan form konfigurasi_peminjaman
    public function update($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);  
		
		// Menampilkan data berdasarkan id-nya yaitu username
        $row = $this->Konfigurasi_peminjaman_model->get_by_id($id);
		
		// Jika id-nya dipilih maka data tahun akademik semester ditampilkan ke form edit konfigurasi_peminjaman
        if ($row) {
            $data = array(
                'button' => 'Update',
				'back'   => site_url('konfigurasi_peminjaman'),
                'action' => site_url('konfigurasi_peminjaman/update_action'),		  
				'id_peminjam' => set_value('id_peminjam', $row->id_peminjam),
				'deskripsi_peminjam' => set_value('deskripsi_peminjam', $row->deskripsi_peminjam),
				'jumlah_hari' => set_value('jumlah_hari', $row->jumlah_hari),			  
				'denda' => set_value('denda', $row->denda),			  
			);
			$this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data konfigurasi_peminjaman
            $this->load->view('konfigurasi_peminjaman/konfigurasi_peminjaman_form', $data); // Menampilkan form tahun akademik semester
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika id-nya yang dipilih tidak ada maka akan menampilkan pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('konfigurasi_peminjaman'));
        }
    }
    
	// Fungsi untuk melakukan aksi update data
    public function update_action(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Rules atau aturan bahwa setiap form harus diisi
        $this->_rules();
		
		// Jika form konfigurasi_peminjaman belum diisi dengan benar 
		// maka sistem akan meminta user untuk menginput ulang
        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('username', TRUE));
        } 	
		// Jika form konfigurasi_peminjaman telah diisi dengan benar 
		// maka sistem akan melakukan update data tahun akademik semester kedalam database
		else {
            $data = array(
			'id_peminjam' => $this->input->post('id_peminjam',TRUE),
			'deskripsi_peminjam' => $this->input->post('deskripsi_peminjam',TRUE),
			'jumlah_hari' => $this->input->post('jumlah_hari',TRUE),
			'denda' => $this->input->post('denda',TRUE),
	    );

            $this->Konfigurasi_peminjaman_model->update($this->input->post('id_peminjam',TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('konfigurasi_peminjaman'));
        }
    }
    
	// Fungsi untuk melakukan aksi delete data berdasarkan id yang dipilih
    public function delete($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
        $row = $this->Konfigurasi_peminjaman_model->get_by_id($id);
		
		//jika id konfigurasi_peminjaman yang dipilih tersedia maka akan dihapus
        if ($row) {
            $this->Konfigurasi_peminjaman_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('konfigurasi_peminjaman'));
        } 
		//jika id konfigurasi_peminjaman yang dipilih tidak tersedia maka akan muncul pesan 'Record Not Found'
		else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('konfigurasi_peminjaman'));
        }
    }
	
	// Fungsi rules atau aturan untuk pengisian pada form (create/input dan update)
    public function _rules() 
    {
	$this->form_validation->set_rules('deskripsi_peminjam', 'deskripsi_peminjam', 'trim|required');	
	$this->form_validation->set_rules('jumlah_hari', 'jumlah_hari', 'trim|required');	
	$this->form_validation->set_rules('denda', 'denda', 'trim|required');	
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

?>