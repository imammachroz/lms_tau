<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
			
			<!-- Menampilkan Data Mahasiswa -->
			<div class="row" style="margin-bottom: 10px">
			
				<div class="col-md-4">
					<h2 style="margin-top:0px">Report Peminjaman</h2>
				</div>
				<div class="col-md-4 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
			</div>
			<div class="row">
        <div class="col-md-12 offset-md-2">
          <!-- <h4 class="mb-3">Filtering</h4> -->
          <div class="row input-daterange">
              <div class="col-md-6 mb-3">
                <label for="firstName">Tanggal Awal</label>
                <input type="text" autocomplete="off" class="form-control" id="start_date" name="start_date" placeholder="" value="" required>
               
              </div>
              <div class="col-md-6">
                <label for="lastName">Tanggal Akhir</label>
                <input type="text"autocomplete="off" class="form-control" id="end_date" name="end_date" placeholder="" value="" required>
                
              </div>
          </div>
		  <br/>
          <div class="">
            <input type="button" name="search" id="search" value="Search" class="btn btn-primary form-control"/>
          </div>
        </div>
        
      </div>
      <br/>
      <br/>
      <div class="row">
        <div class="col-md-12 offset-md-2">
          <!-- <h4 class="mb-3">Data</h4> -->
          <table id="table" class="table table-bordered table-striped">
            <thead>
              <tr>
              <th width=5%>No</th>
              <th>Tanggal</th>
              <th>NIM/NIK</th>
              <th>Nama Anggota</th>
              <th>Kode Buku</th>
              <th>Judul Buku</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>

   
    <script src="assets/js/resp/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="assets/js/resp/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js" crossorigin="anonymous"></script>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />

    
    <script src="assets/js/resp/dataTables.rowReorder.min.js"></script>
    <script src="assets/js/resp/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
    
    <link rel="stylesheet" type="text/css" href="assets/css/rowReorder.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.dataTables.min.css"/>
    

    <link rel="stylesheet"  href="assets/vendor/DataTables/buttons.datatables.min.css">    
    <script src="assets/vendor/DataTables/dataTables.buttons.min.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/jszip.min.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/pdfmake.min.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/vfs_fonts.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/buttons.html5.min.js" type="text/javascript"></script> 

  <script type="text/javascript" language="javascript" >
      $(document).ready(function(){
        $('.input-daterange').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
          {
              return {
                  "iStart": oSettings._iDisplayStart,
                  "iEnd": oSettings.fnDisplayEnd(),
                  "iLength": oSettings._iDisplayLength,
                  "iTotal": oSettings.fnRecordsTotal(),
                  "iFilteredTotal": oSettings.fnRecordsDisplay(),
                  "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                  "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
              };
          };
          
        var table;
        table=$('table').DataTable({
          initComplete: function() {
              var api = this.api();
              $('#table_filter input')
                      .off('.DT')
                      .on('keyup.DT', function(e) {
                          if (e.keyCode == 13) {
                              api.search(this.value).draw();
                  }
              });
          },
					dom: '<"top"lf><tr><"bottom"Bip>',
						oLanguage: {
							sProcessing: "<div id='loader'></div>"
						},
						processing: true,
						serverSide: false,
          bAutoWidth : false, 
          responsive: true,
          rowReorder: {
                  selector: 'td:nth-child(0)'
          },
          processing: true,
          serverSide: false,
          ajax: {"url": "report_peminjaman/json", "type": "POST"},
          columns: [
                {
                    "data": "num",
                    "orderable": false
                },						
                    {"data": "tanggal_pinjam"},	
                    {"data": "nim"},	
                    {"data": "nama_lengkap"},	
                    {"data": "kode_buku"},	
                    {"data": "judul_buku"},	
            ],
          order: [[0, 'asc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              var index = page * length + (iDisplayIndex + 1);
              $('td:eq(0)', row).html(index);
          },
          "buttons": [
            {extend: 'copy'}, 'csv', 'excel', 'pdf',
        ],
        });

        $('#search').click(function(){
          var start_date = $('#start_date').val();
          var end_date = $('#end_date').val();
          if(start_date != '' && end_date !='')
            { 
              if ( $.fn.DataTable.isDataTable('table')) {
                  table.destroy();
              }
            table=$('table').DataTable({
              dom: '<lf<tr>ipB>',
						oLanguage: {
							sProcessing: "<div id='loader'></div>"
						},
						processing: true,
						serverSide: false,
              bAutoWidth : false, 
              responsive: true,
              rowReorder: {
                      selector: 'td:nth-child(0)'
              },
              "ajax" : {
                url:"<?=site_url("Report_peminjaman/search")?>",
                type:"POST",
                data:{
                  start_date:start_date, end_date: end_date
                }
              },
              
              "columns": [
                    {
                      "data": "num"
                    },
                    {"data": "tanggal_pinjam"},	
                    {"data": "nim"},	
                    {"data": "nama_lengkap"},	
                    {"data": "kode_buku"},	
                    {"data": "judul_buku"},	
              
            ],
            
            order: [[0, 'asc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            },
              "buttons": [
                {extend: 'copy'}, 'csv', 'excel', 'pdf',
            ],
            });
          }
          else
          {
            alert("Both Date is Required");
          }
        }); 
      });
    </script>
