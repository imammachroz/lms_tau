<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>        
        <li class="active">Detail Peminjaman</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form KRS-->
			<center>
			<legend><strong>DETAIL PEMINJAMAN</strong>
				</legend>
				<table>
					<tr>
						<td1><strong>NIM </strong></td1> <td1> &nbsp; <?php echo $nim; ?></td1>
					<tr>
						<br/>
					<tr>
						<td1><strong>Nama Mahasiswa </strong></td1> <td1> &nbsp; <?php echo $nama; ?></td1>
					<tr>
				</table>
			</center>
			<br />
			<div style="overflow-x:auto;">
			<table class="table table-bordered table-striped" style="margin-bottom: 10px;">
				<thead>
					<tr >
						<th scope="col" >No</th>	
						<th scope="col" >Kode Buku</th>
						<th scope="col"  >Judul Buku</th>
						<th scope="col" >Tanggal Pinjam</th>
						<th scope="col" >Tanggal Kembali</th>
						<th scope="col" >Denda</th>
					</tr>
					</thead>	
				<?php
				  $no=1; // Nomor urut dalam menampilkan data
				  //$jumlahSks=0; // Jumlah SKS dimulai dari 0
				  
				  // Menampilkan data KRS
				  foreach($peminjaman_data as $peminjaman)
				  {
				?>
				<tr>
					 <td data-header="No" class="title" width="80px"><?php echo $no++; ?></td>
					 <td data-header="Kode Buku"><?php echo $peminjaman->kode_buku;?></td>
					 <td data-header="Judul Buku"><?php echo $peminjaman->judul_buku;?></td>
					 <td data-header="Tanggal Pinjam"><?php echo $peminjaman->tanggal_pinjam;?></td>
					 <td data-header="Tanggal Kembali"><?php echo $peminjaman->tanggal_kembali;?></td>
					 <td data-header="Denda">Rp. <?php echo $denda=$peminjaman->denda;?></td>
				</tr>
				<?php
					}
				?>
				</table>
				<div>    
			 <?php 
				// Button untuk melakukan create KRS
				echo anchor(site_url('admin'),'Back', 'class="btn btn-primary"'); 
			 ?>
			</div>
