<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buku extends CI_Controller {


	public function __construct(){
		parent::__construct();

		//$this->load->model('Buku_model');
	}

	public function index(){

		$data['title']='Home Perpustakaan';
		$tmp['content']=$this->load->view('global/home', $data,TRUE);
		$this->load->view('global/layout',$tmp);
		
	}

	//menampilkan daftar buku
	public function list_buku(){
		 
		$data['title']='Daftar buku';
			/*data yang ditampilkan*/
			
			$data['data_buku'] = $this->Buku_model->getAllData($this->input->get('filter_kategori'));
			$data['model'] = $this->Buku_model;
			$data['kategori'] = $this->Buku_model->getAllCategory();
			/*masukan data kedalam view */
			//$data['js']=$this->load->view('admin/buku/js');
			$tmp['content']=$this->load->view('global/R_buku',$data, TRUE);
			$this->load->view('global/layout',$tmp);
		}

	
}
