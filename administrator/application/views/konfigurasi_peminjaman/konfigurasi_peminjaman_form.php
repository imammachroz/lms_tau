<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Konfigurasi_peminjaman</a></li>
        <li class="active"><?php echo $button ?> Konfigurasi_peminjaman</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input atau edit Konfigurasi_peminjaman -->
			<h2 style="margin-top:0px"><?php echo $button ?> Konfigurasi Peminjaman </h2>
			<form action="<?php echo $action; ?>" method="post">
			<input type="hidden"  class="form-control" name="id_peminjam" id="id_peminjam" value="<?php echo $id_peminjam; ?>" />
				<div class="form-group">
					<label for="varchar">Deskripsi Peminjam <?php echo form_error('deskripsi_peminjam') ?></label>
					<input type="text" class="form-control" name="deskripsi_peminjam" id="deskripsi_peminjam" placeholder="Deskripsi Peminjam" value="<?php echo $deskripsi_peminjam; ?>" />
				</div>
				<div class="form-group">
					<label for="varchar">Jumlah Hari <?php echo form_error('jumlah_hari') ?></label>
					<input type="text" class="form-control" name="jumlah_hari" id="jumlah_hari" placeholder="Jumlah Hari" value="<?php echo $jumlah_hari; ?>" />
				</div>
				<div class="form-group">
					<label for="varchar">Denda <?php echo form_error('denda') ?></label>
					<input type="text" class="form-control" name="denda" id="jumlah_hari" placeholder="Denda" value="<?php echo $denda; ?>" />
				</div>
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
				<a href="<?php echo site_url('konfigurasi_peminjaman') ?>" class="btn btn-default">Cancel</a>
			</form>
    