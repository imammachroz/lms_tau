<?php
/*****************************************************/
/* File        : Peminjaman.php                           */
/* Lokasi File : ./application/controllers/Peminjaman.php */
/* Copyright   : Yosef Murya & Badiyanto             */
/* Publish     : Penerbit Langit Inspirasi           */
/*---------------------------------------------------*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Peminjaman
class Peminjaman extends CI_Controller
{
  // Fungsi untuk menampilkan halaman peminjaman 
   // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Peminjaman_model'); // Memanggil Peminjaman_model yang terdapat pada models
		$this->load->model('Users_model');
		$this->load->model('Mahasiswa_model');
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library        
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman peminjaman
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);  		
		$this->load->view('header_list',$dataAdm); // Menampilkan bagian header dan object data peminjaman
        $this->load->view('peminjaman/peminjaman_list'); // Menampilkan halaman peminjaman
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 
    
	// Fungsi JSON
    public function json() {
        header('Content-Type: application/json');
		echo $this->Peminjaman_model->json1();
	}
	public function json_peminjaman_mahasiswa() {
        header('Content-Type: application/json');
		echo $this->Mahasiswa_model->json_peminjaman_mahasiswa();
	}
	
    public function baca_peminjaman($id)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login			
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }
		$this->db->select('mahasiswa.id_mahasiswa, peminjaman.id_peminjaman, peminjaman.id_buku,
		buku.kode_buku,judul.judul_buku,peminjaman.tanggal_pinjam
		 ,peminjaman.tanggal_kembali, peminjaman.is_kembali
		 , case when datediff(CURDATE(),peminjaman.tanggal_kembali) < 0 
		 then 0 else datediff(CURDATE(),peminjaman.tanggal_kembali) * konfigurasi_peminjaman.denda end as denda');
		$this->db->from('mahasiswa');
		$this->db->join('peminjaman','mahasiswa.id_mahasiswa=peminjaman.id_mahasiswa');
		$this->db->join('konfigurasi_peminjaman','mahasiswa.id_peminjam=konfigurasi_peminjaman.id_peminjam');
		$this->db->join('buku','buku.id_buku=peminjaman.id_buku');
		$this->db->join('judul','judul.id_judul=buku.id_judul');
		$this->db->where('is_kembali','T');
		$this->db->where('mahasiswa.id_mahasiswa',$id);
	  	$peminjaman = $this->db->get()->result();
		return $peminjaman;
	}
	
	// Fungsi untuk menampilkan halaman peminjaman secara detail
    public function read($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
			'id_mahasiswa' => $rowAdm->id_mahasiswa,
			'nama_panggilan' => $rowAdm->nama_panggilan,
			'photo' => $rowAdm->photo,
		);
		
		// Menampilkan data peminjaman yang ada di database berdasarkan id-nya yaitu id_peminjaman
        $row = $this->Peminjaman_model->get_by_id($id);

		 $id_mahasiswa=$this->input->post('id_mahasiswa',TRUE);
		 $id_peminjaman=$this->input->post('id_peminjaman',TRUE);
         // Menampung data yang diinputkan 	
         $data = array('action' => site_url('peminjaman/peminjaman_action'),
              'id_mahasiswa'=>$id_mahasiswa,
            );
        
		// Jika data peminjaman tersedia maka akan ditampilkan
        if ($row) {
            $dataPeminjam=array(
                'button' => 'Create',
                'back'   => site_url('peminjaman'),
				'peminjaman_data'=>$this->baca_peminjaman($id),
				'id_mahasiswa'=>$this->Mahasiswa_model->get_by_id($id)->id_mahasiswa,
				'nim'=>$this->Mahasiswa_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id)->id_mahasiswa)->nim,
				'nama'=>$this->Mahasiswa_model->get_by_id(
					$this->Peminjaman_model->get_by_id($id)->id_mahasiswa)->nama_lengkap,
                );
            $this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
			$this->load->view('peminjaman/peminjaman_read', $dataPeminjam); // Menampilkan halaman detail peminjaman
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika data peminjaman tidak tersedia maka akan ditampilkan informasi 'Record Not Found'
		else {
			$this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
            $this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('peminjaman'));
			$this->load->view('footer'); // Menampilkan bagian footer
        }
    }
}
?>