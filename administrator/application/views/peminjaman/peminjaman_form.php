<head>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>
<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
			
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('/judul/read/'.$id_mahasiswa) ?>"> Data Buku</a></li>
        <li class="active"><?php echo $this->Anggota_model->get_by_id($id_mahasiswa)->nama_lengkap ?></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
		

			<!-- Form input dan edit data KRS-->
			<legend>Input buku baru <?php echo $this->Anggota_model->get_by_id($id_mahasiswa)->nama_lengkap ;?>  </legend>
			
			<form action="<?php echo $action; ?>" method="post">
			 <div class="form-group">
				<!-- <label for="int">ID Judul <?php echo form_error('id_judul') ?></label> -->
				<input type="hidden" class="form-control" name="id_mahasiswa" value="<?php echo $id_mahasiswa; ?>"  />
					<div class="col-sm-6">	
						<select class="itemName form-control"  id="id_buku" name="id_buku" value="<?php echo $id_buku; ?>"> </select>
					</div>
				<script type="text/javascript">
				$('.itemName').select2({
					placeholder: '--- Pilih Judul ---',
					//minimumInputLength: 1,
					//allowClear: true,
					width: '100%',
					ajax: {
						url: '../search_book',
						dataType: 'json',
						delay: 250,
						data : function(params){
							return{
								buku:params.term
							};
						},
						processResults: function (data) {
							var results = [];
							
							$.each(data, function(index,item,item1){
								results.push({
									id: item.id_buku,
									text: item.kode_buku +' - '+ item.judul_buku
								});
							});
							return {
								results: results
							};
						}
					}
				});
				</script>
			 </div>
			 <br/><br/>
				<div class="col-sm-4">
			 <button type="submit" class="btn btn-primary">Simpan</button> 
					<a href="<?php echo site_url('/peminjaman/read/'.$id_mahasiswa) ?>" class="btn btn-default">Cancel</a>
			</div>
			</form>


   