<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model
 {
    function json() {
            
        $this->datatables->select('(@row_number:=@row_number + 1) num, judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, 
        kategori.nama_kategori ,penerbit.nama_penerbit as penerbit, penulis.nama_penulis as penulis, COUNT(buku.id_buku) as stok, 
        (CASE WHEN stok_tersedia.stok_tersedia is null then 0 ELSE stok_tersedia.stok_tersedia END) 
        stok_tersedia, (CASE WHEN stok_dipinjam.stok_dipinjam is null then 0 
        ELSE stok_dipinjam.stok_dipinjam END) stok_dipinjam');
        $this->datatables->from('buku, (SELECT @row_number:=0) AS t');
        $this->datatables->join('judul','judul.id_judul=buku.id_judul');
        $this->datatables->join('kelas','judul.id_kelas=kelas.id_kelas');
        $this->datatables->join('kategori','kategori.id_kategori=judul.id_kategori');
        $this->datatables->join('penerbit','penerbit.id_penerbit=judul.id_penerbit');
        $this->datatables->join('penulis','penulis.id_penulis=judul.id_penulis');
        $this->datatables->join('(
            SELECT judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, kategori.nama_kategori,
            penerbit.nama_penerbit , penulis.nama_penulis, COUNT(buku.id_buku) as stok_tersedia
            from buku
            LEFT JOIN judul on buku.id_judul=judul.id_judul 
            LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
            LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
            LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
            LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
            WHERE buku.is_ada="Tersedia"
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
            penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
            ) stok_tersedia','stok_tersedia.id_judul=buku.id_judul','LEFT',NULL);
        $this->datatables->join('(
            SELECT judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, kategori.nama_kategori,
            penerbit.nama_penerbit , penulis.nama_penulis, COUNT(buku.id_buku) as stok_dipinjam
            from buku
            LEFT JOIN judul on buku.id_judul=judul.id_judul 
            LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
            LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
            LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
            LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
            WHERE buku.is_ada="Tidak"
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
            penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
            ) stok_dipinjam','stok_dipinjam.id_judul=buku.id_judul', 'LEFT',NULL);
        $this->datatables->group_by('judul.id_judul, judul.isbn, judul.judul_buku, 
        penulis.nama_penulis, penerbit.nama_penerbit, kelas.nama_kelas, kategori.nama_kategori');
        return $this->datatables->generate();
    }

    function json_report_peminjaman(){
        $this->datatables->select("(@row_number:=@row_number + 1) num,id_peminjaman, tanggal_pinjam,mahasiswa.nim,mahasiswa.nama_lengkap,
        buku.kode_buku, judul.judul_buku");
        $this->datatables->from('peminjaman, (SELECT @row_number:=0) AS t');
        $this->datatables->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->datatables->join('buku','peminjaman.id_buku=buku.id_buku');
        $this->datatables->join('judul','judul.id_judul=buku.id_judul');
        return $this->datatables->generate();
    }
    
    function json_report_pengembalian(){
        $this->datatables->select("(@row_number:=@row_number + 1) num,id_peminjaman, tanggal_kembali,mahasiswa.nim,mahasiswa.nama_lengkap,
        buku.kode_buku, judul.judul_buku");
        $this->datatables->from('peminjaman, (SELECT @row_number:=0) AS t');
        $this->datatables->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->datatables->join('buku','peminjaman.id_buku=buku.id_buku');
        $this->datatables->join('judul','judul.id_judul=buku.id_judul');
        return $this->datatables->generate();
    }

    function json_report_denda(){
        $this->datatables->select("(@row_number:=@row_number + 1) num,id_peminjaman, tanggal_kembali,mahasiswa.nim,mahasiswa.nama_lengkap,
        denda");
        $this->datatables->from('peminjaman, (SELECT @row_number:=0) AS t');
        $this->datatables->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->datatables->where('denda >','0');
        return $this->datatables->generate();
    }

    public function getAllPeminjamanData(){
        return $this->db->get('peminjaman')->result();
    }

    public function filter_tanggal_peminjaman( $start_date, $end_date){
        $this->db->select("(@row_number:=@row_number + 1) num, id_peminjaman, tanggal_pinjam,mahasiswa.nim,mahasiswa.nama_lengkap,
        buku.kode_buku, judul.judul_buku");
        $this->db->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->db->join('buku','peminjaman.id_buku=buku.id_buku');
        $this->db->join('judul','judul.id_judul=buku.id_judul');
        $this->db->where('tanggal_pinjam >=',$start_date); 
        $this->db->where('tanggal_pinjam <=',$end_date);
        return $this->db->get('peminjaman, (SELECT @row_number:=0) AS t')->result();
    }

    public function filter_tanggal_pengembalian( $start_date, $end_date){
        $this->db->select("(@row_number:=@row_number + 1) num, id_peminjaman, tanggal_pinjam,mahasiswa.nim,mahasiswa.nama_lengkap,
        buku.kode_buku, judul.judul_buku");
        $this->db->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->db->join('buku','peminjaman.id_buku=buku.id_buku');
        $this->db->join('judul','judul.id_judul=buku.id_judul');
        $this->db->where('tanggal_kembali >=',$start_date); 
        $this->db->where('tanggal_kembali <=',$end_date);
        return $this->db->get('peminjaman, (SELECT @row_number:=0) AS t')->result();
    }

    public function filter_tanggal_denda( $start_date, $end_date){
        $this->db->select("(@row_number:=@row_number + 1) num, id_peminjaman, tanggal_kembali,mahasiswa.nim,mahasiswa.nama_lengkap,
        denda");
        $this->db->join('mahasiswa','peminjaman.id_mahasiswa=mahasiswa.id_mahasiswa');
        $this->db->where('denda >','0');
        $this->db->where('tanggal_kembali >=',$start_date); 
        $this->db->where('tanggal_kembali <=',$end_date);
        return $this->db->get('peminjaman, (SELECT @row_number:=0) AS t')->result();
    }
    

    public function getAllCategory()
    {
        $query = $this->db->query('SELECT id_kategori,nama_kategori FROM kategori');
        return $query->result_array();
    }

    function report(){
        $query = $this->db->query("SELECT judul.id_judul, judul.isbn, 
        (CASE WHEN char_length(judul.judul_buku) > 35 
        then concat(SUBSTRING(judul.judul_buku,1,35),' ...')
        else judul.judul_buku end ) as judul_buku , 
        (CASE WHEN char_length(penerbit.nama_penerbit) > 20 
        then concat(SUBSTRING(penerbit.nama_penerbit,1,20),' ...')
        else penerbit.nama_penerbit end ) as penerbit ,
        (CASE WHEN char_length(penulis.nama_penulis) > 20 
        then concat(SUBSTRING(penulis.nama_penulis,1,20),' ...')
        else penulis.nama_penulis end ) as penulis, COUNT(buku.id_buku) as stok, 
        (CASE WHEN stok_tersedia.stok_tersedia is null then 0 ELSE stok_tersedia.stok_tersedia END) 
        stok_tersedia, 
        (CASE WHEN stok_dipinjam.stok_dipinjam is null then 0 ELSE stok_dipinjam.stok_dipinjam END) 
        stok_dipinjam
        from buku 
        LEFT JOIN judul on buku.id_judul=judul.id_judul 
        LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
        LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
        LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
        LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
        LEFT JOIN (
            SELECT judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, kategori.nama_kategori ,
            penerbit.nama_penerbit , penulis.nama_penulis, COUNT(buku.id_buku) as stok_tersedia
            from buku
            LEFT JOIN judul on buku.id_judul=judul.id_judul 
            LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
            LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
            LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
            LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
            WHERE buku.is_ada='Tersedia'
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
            penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
            ) stok_tersedia ON stok_tersedia.id_judul=buku.id_judul
        LEFT JOIN (
            SELECT judul.id_judul, judul.isbn, judul.judul_buku, kelas.nama_kelas, kategori.nama_kategori ,
            penerbit.nama_penerbit , penulis.nama_penulis, COUNT(buku.id_buku) as stok_dipinjam
            from buku
            LEFT JOIN judul on buku.id_judul=judul.id_judul 
            LEFT JOIN kelas on judul.id_kelas=kelas.id_kelas 
            LEFT JOIN kategori on kategori.id_kategori=judul.id_kategori
            LEFT JOIN penerbit on penerbit.id_penerbit=judul.id_penerbit
            LEFT JOIN penulis on penulis.id_penulis=judul.id_penulis
            WHERE buku.is_ada='Tidak'
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
            penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
            ) stok_dipinjam ON stok_dipinjam.id_judul=buku.id_judul
            GROUP BY judul.id_judul, judul.isbn, judul.judul_buku, penulis.nama_penulis, 
            penerbit.nama_penerbit, kelas.nama_kelas, 	kategori.nama_kategori
        ORDER BY judul.judul_buku");
        return $query;
    }
 }

        