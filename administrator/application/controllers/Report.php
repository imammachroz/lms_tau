<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Controller
{
     // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Report_model'); // Memanggil Kategori_model yang terdapat pada models
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library 
        $this->load->library('pdf');
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper		
		$this->load->helper('my_function'); // Memanggil fungsi my_function yang terdapat pada helper		
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman kategori
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
        );
		
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('report/report_list'); // Menampilkan halaman utama kategori
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 

    // Fungsi JSON
	public function json() {
        header('Content-Type: application/json');
        echo $this->Report_model->json();
    }

    function report_pdf(){
        $pdf = new FPDF('l','mm','A4');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Cell(275,7,'Laporan Data Buku Perpustakaan',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(275,7,'Tanri Abeng University',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(272,5,'Update Per Tanggal : ',0,1,'R');
        $pdf->Cell(270,4,($date = date('m/d/Y')),0,1,'R');
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(10,3,'',0,1);
        $pdf->Cell(8,6,'No',1,0);
        $pdf->Cell(30,6,'ISBN',1,0);
        $pdf->Cell(80,6,'Judul Buku',1,0);
        $pdf->Cell(45,6,'Penerbit',1,0);
        $pdf->Cell(50,6,'Penulis',1,0);
        $pdf->Cell(20,6,'Stok Buku',1,0);
        $pdf->Cell(20,6,'Tersedia',1,0);
        $pdf->Cell(20,6,'Dipinjam',1,1);
        $pdf->SetFont('Arial','',10);
        $no=1;
        foreach ($this->Report_model->report()->result() as $row){
            $pdf->Cell(8,6,$no++,1,0);
            $pdf->Cell(30,6,$row->isbn,1,0);
            $pdf->Cell(80,6,$row->judul_buku,1,0);
            $pdf->Cell(45,6,$row->penerbit,1,0);
            $pdf->Cell(50,6,$row->penulis,1,0); 
            $pdf->Cell(20,6,$row->stok,1,0); 
            $pdf->Cell(20,6,$row->stok_tersedia,1,0); 
            $pdf->Cell(20,6,$row->stok_dipinjam,1,1); 
        }
        $pdf->Output();
    }

}
?>