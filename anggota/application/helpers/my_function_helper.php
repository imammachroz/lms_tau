<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Fungsi untuk membuat tanggal dengan format Indonesia
function tgl_indo($tgl){
	$tanggal = substr($tgl,8,2);
	$bulan = getBulan(substr($tgl,5,2));
	$tahun = substr($tgl,0,4);
	return $tanggal.' '.$bulan.' '.$tahun;		 
}
// function encrypt($payload) {
// 	$key = '1234567891011120';
// 	$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
// 	$encrypted = openssl_encrypt($payload, 'aes-256-cbc', $key, 0, $iv);
// 	return base64_encode($encrypted . '::' . $iv);
//   }
function encrypt_pass($payload) {
	$encrypt_method = "AES-256-CBC";
    $secret_iv = 'lms-tau-by-imam-machroz';
    // hash
    $key = '2803201905011996';
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	$encrypted = openssl_encrypt($payload, $encrypt_method, $key, 0, $iv);
	return base64_encode($encrypted . '::' . $iv);
  }
function decrypt_pass($garble) {

	$encrypt_method = "AES-256-CBC";
    $secret_iv = 'lms-tau-by-imam-machroz';
    // hash
    $key = '2803201905011996';
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
	$iv = substr(hash('sha256', $secret_iv), 0, 16);
	list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
    return openssl_decrypt($encrypted_data, $encrypt_method, $key, 0, $iv);
}

// function decrypt($garble) {
// 	$key = '2803201905011996';
//     list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
//     return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
// }
// Fungsi untuk membuat bulan dengan format Indonesia
function getBulan($bln){
				switch ($bln){
					case 1: 
						return "Januari";
						break;
					case 2:
						return "Februari";
						break;
					case 3:
						return "Maret";
						break;
					case 4:
						return "April";
						break;
					case 5:
						return "Mei";
						break;
					case 6:
						return "Juni";
						break;
					case 7:
						return "Juli";
						break;
					case 8:
						return "Agustus";
						break;
					case 9:
						return "September";
						break;
					case 10:
						return "Oktober";
						break;
					case 11:
						return "November";
						break;
					case 12:
						return "Desember";
						break;
				}
	}
	

// Fungsi untuk melakukan input data	
function inputtext($name, $table, $field, $primary_key, $selected){
	$ci = get_instance();
	$data = $ci->db->get($table)->result();
	foreach($data as $t){
		if($selected == $t->$primary_key){
		$txt = $t->$field;
		}
	}
	return $txt;
}

// Fungsi untuk menampilkan data dalam bentuk combobox
function combobox($name, $table, $field, $primary_key, $selected){
	$ci = get_instance();
	$cmb = "<select name='$name' class='form-control'>";
	$data = $ci->db->get($table)->result();
	$cmb .="<option value=''>-- PILIH --</option>";
	foreach($data as $d){		
		$cmb .="<option value='".$d->$primary_key."'";
		$cmb .= $selected==$d->$primary_key?"selected='selected'":'';
		$cmb .=">". strtoupper($d->$field)."</option>";
	}
	$cmb .="</select>";
	return $cmb;
}

// Fungsi untuk mengkonversi nilai angka kedalam bentuk abjad
function skorNilai($nilai,$sks)
 {
	if ($nilai=='A') $skor=4*$sks;
	else if ($nilai=='B') $skor=3*$sks;
	    else if ($nilai=='C') $skor=2*$sks;
	         else if ($nilai=='D') $skor=1*$sks;
			      else $skor=0;
		return $skor;	 
 }
 
// Fungsi untuk melakukan cek nilai 
function cekNilai($nim, $kode, $nilKhs){
  $ci = get_instance();	 
  $ci->load->model('Transkrip_model');
  
  $ci->db->select('*');
  $ci->db->from('transkrip');
  $ci->db->where('nim', $nim);
  $ci->db->where('kode_matakuliah',$kode);
  $query=$ci->db->get()->row();  
  // Jika nilai tidak kosong atau isi
  if ($query!=null) 
  {   
	 // Membandingkan jika terdapat nilai sebelumnya di matakuliah yang sama
	 // jika nilai yang diinputkan lebih besar dari nilai sebelumnya
	 // secara otomatis nilai lama yg lebih kecil akan diganti dengan nilai yang lebih besar
     if ($nilKhs < $query->nilai) 
	 {      
	  $ci->db->set('nilai',$nilKhs)
         ->where('nim',$nim)
		 ->where('kode_matakuliah',$kode)		 
         ->update('transkrip');	 
	 }	
  }	 
  // Jika nilai belum ada maka tambahkan nilai baru
  else 
  { 
	 $data =array('nim'=>$nim,
                  'nilai'=>$nilKhs,
                  'kode_matakuliah'=>$kode);
	 $ci->Transkrip_model->insert($data);	  
  }	 
  
 }
//fungsi SEO
function seo_title($s) {
    $c = array (' ');
    $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+');

    $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
    
    $s = strtolower(str_replace($c, '-', $s)); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
    return $s;
} 

function adddate($hari) {
	
	$waktu 		= $hari * 2;
	$CI 		=& get_instance();
	$q_ambil	= $CI->db->query("SELECT tanggal FROM libur ORDER BY id_libur ASC")->result();
	$jum_libnas	= 0;
	$jum_hari	= 0;
	$jumlah_minggu	= 0;
	
	for ($i = 0; $i <= $waktu; $i++) {
		$tgl	= mktime(0, 0, 0, date("n"), date("j")+$i,  date("Y"));
		foreach($q_ambil as $q) {
			if ($q->tanggal == date('Y-m-d', $tgl)) {
				$jum_libnas++;
			}			
		}
	
	
	if (date("l", $tgl) == "Sunday" or date("l", $tgl) == "Saturday" ) {
		$jumlah_minggu++;
	} else {
		if ($jum_hari == $hari) {
			break;
		} else {
			$jum_hari++;
		}
	}
	}
	
	$jumhari	= $jum_hari + $jum_libnas + $jumlah_minggu;
	$nextday	= mktime(0,0,0,date("n"),date("j")+$jumhari,date("Y"));
	
	return date("Y-m-d", $nextday);
}

