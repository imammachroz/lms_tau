<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Menampilkan Data Mahasiswa -->
			<div class="row" style="margin-bottom: 10px">
				<div class="col-md-4">
					<h2 style="margin-top:0px">Report Koleksi</h2>
				</div>
				<div class="col-md-4 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
				<!-- <div class="col-md-4 text-right">
					<?php echo anchor(site_url('report/report_pdf'), '<i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print PDF', 'class="btn btn-success"'); ?>
			</div> -->
			</div>
			<table class="table table-bordered table-striped" id="mytable">
				<thead>
					<tr>
                        <th width="5%">No</th>
                        <th>ISBN</th>
                        <th width="15%">Judul Buku</th>
                        <th>Kelas</th>
                        <th>Kategori</th>
                        <th>Penerbit</th>
                        <th>Pengarang</th>
                        <th>Total Stok</th>
                        <th>Tersedia</th>
                        <th>Dipinjam</th>
					</tr>
				</thead>
 
            <tbody>
                
            </tbody>
				 </table>

   
<!-- Memanggil jQuery -->
			
			<script src="assets/js/resp/jquery-3.3.1.js" crossorigin="anonymous"></script>
			<script src="assets/js/resp/jquery.dataTables.min.js" crossorigin="anonymous"></script>
			<script src="assets/js/resp/dataTables.rowReorder.min.js" crossorigin="anonymous"></script>
			<script src="assets/js/resp/dataTables.responsive.min.js" crossorigin="anonymous"></script>

			<link rel="stylesheet" type="text/css" href="assets/css/rowReorder.dataTables.min.css"/>
			<link rel="stylesheet" type="text/css" href="assets/css/responsive.dataTables.min.css"/>
			<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>

			
		<link rel="stylesheet"  href="assets/vendor/DataTables/buttons.datatables.min.css">    
    <script src="assets/vendor/DataTables/dataTables.buttons.min.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/jszip.min.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/pdfmake.min.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/vfs_fonts.js" type="text/javascript"></script> 
    <script src="assets/vendor/DataTables/buttons.html5.min.js" type="text/javascript"></script> 

			<!-- JavaScript yang berfungsi untuk menampilkan data dari tabel mahasiswa dengan AJAX -->
			<script type="text/javascript">
				$(document).ready(function() {
					$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
					{
						return {
							"iStart": oSettings._iDisplayStart,
							"iEnd": oSettings.fnDisplayEnd(),
							"iLength": oSettings._iDisplayLength,
							"iTotal": oSettings.fnRecordsTotal(),
							"iFilteredTotal": oSettings.fnRecordsDisplay(),
							"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
							"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
						};
					};
					
					var t = $("#mytable").dataTable({
						initComplete: function() {
							var api = this.api();
							$('#mytable_filter input')
									.off('.DT')
									.on('keyup.DT', function(e) {
										if (e.keyCode == 13) {
											api.search(this.value).draw();
										}
							});
						},
						dom: '<"top"lf><tr><"bottom"Bip>',
						oLanguage: {
							sProcessing: "<div id='loader'></div>"
						},
						bAutoWidth : false, 
						responsive: true,
						rowReorder: {
								selector: 'td:nth-child(0)'
						},
						processing: true,
						serverSide: false,
						ajax: {"url": "report/json", "type": "POST" },
						columns: [
							{
								"data": "num",
								"orderable": false
                            },
								{"data": "isbn"},
								{"data": "judul_buku"},
								{"data": "nama_kelas"},
								{"data": "nama_kategori"},
								{"data": "penerbit"},
								{"data": "penulis"},
								{"data": "stok", "searchable": false},
								{"data": "stok_tersedia","searchable": false},
                {"data": "stok_dipinjam","searchable": false},
                            
                        ],
              "buttons": [
                {extend: 'copy'}, 'csv', 'excel', 'pdf',
            	],
						order: [[0, 'asc']],
						rowCallback: function(row, data, iDisplayIndex) {
							var info = this.fnPagingInfo();
							var page = info.iPage;
							var length = info.iLength;
							var index = page * length + (iDisplayIndex + 1);
							$('td:eq(0)', row).html(index);
						}
					});
				});
			</script>
  
