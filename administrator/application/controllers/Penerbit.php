<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penerbit extends CI_Controller
{
     // Konstruktor	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Penerbit_model'); // Memanggil Penerbit_model yang terdapat pada models
		$this->load->model('Users_model'); // Memanggil Users_model yang terdapat pada models
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library 
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper		
		$this->load->helper('my_function'); // Memanggil fungsi my_function yang terdapat pada helper		
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman penerbit
    public function index(){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
		$this->load->view('header_list', $dataAdm); // Menampilkan bagian header dan object data users 
        $this->load->view('penerbit/penerbit_list'); // Menampilkan halaman utama penerbit
		$this->load->view('footer_list'); // Menampilkan bagian footer
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Penerbit_model->json();
    }
	
	/*
    public function read($id) 
    {
        $row = $this->Penerbit_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_penerbit' => $row->id_penerbit,
		'nama_penerbit' => $row->nama_penerbit,
		'penerbit_seo' => $row->penerbit_seo,
		'aktif' => $row->aktif,
	    );
            $this->load->view('penerbit/penerbit_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penerbit'));
        }
    }
	*/
	
    public function create() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
        $data = array(
			'back'   => site_url('penerbit'),
            'button' => 'Create',
            'action' => site_url('penerbit/create_action'),
			'id_penerbit' => set_value('id_penerbit'),
			'nama_penerbit' => set_value('nama_penerbit'),		
		);
		
		$this->load->view('header',$dataAdm ); // Menampilkan bagian header dan object data users 	 
        $this->load->view('penerbit/penerbit_form', $data); // Menampilkan halaman form penerbit
		$this->load->view('footer'); // Menampilkan bagian footer
    }
    
    public function create_action() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } 
		else {
            $data = array(
				'id_penerbit'   => $this->input->post('id_penerbit',TRUE),
				'nama_penerbit' => $this->input->post('nama_penerbit',TRUE),
				);           
            $this->Penerbit_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penerbit'));
        }
		
    }
    
    public function update($id) 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
		);
		
        $row = $this->Penerbit_model->get_by_id($id);

        if ($row) {
            $data = array(
				'back'   => site_url('penerbit'),
                'button' => 'Update',
                'action' => site_url('penerbit/update_action'),
				'id_penerbit' => set_value('id_penerbit', $row->id_penerbit),
				'nama_penerbit' => set_value('nama_penerbit', $row->nama_penerbit),
				);
							
			$this->load->view('header',$dataAdm); // Menampilkan bagian header dan object data users 	
            $this->load->view('penerbit/penerbit_form', $data); // Menampilkan form mahasiswa
			$this->load->view('footer'); // Menampilkan bagian footer
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penerbit'));
        }
    }
    
    public function update_action() 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_penerbit', TRUE));
        } else {
            $data = array(
						'id_penerbit'   => $this->input->post('id_penerbit',TRUE),
						'nama_penerbit' => $this->input->post('nama_penerbit',TRUE),
						);
			
            $this->Penerbit_model->update($this->input->post('id_penerbit', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penerbit'));
        }
    }
    
    public function delete($id) 
    {
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
		
        $row = $this->Penerbit_model->get_by_id($id);

        if ($row) {
            $this->Penerbit_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penerbit'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penerbit'));
        }
    }

    public function _rules(){
	$this->form_validation->set_rules('id_penerbit', 'id penerbit', 'trim|required');
	$this->form_validation->set_rules('nama_penerbit', 'nama penerbit', 'trim|required');
	$this->form_validation->set_rules('id_penerbit', 'id_penerbit', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
?>