<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $button ?> Anggota</li>
      </ol>
	</section>
	<head>
        <!-- Latest compiled and minified CSS
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"> -->
        <!-- Latest compiled and minified JavaScript -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/custom.js"></script>
    </head>
	<!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input dan edit Mahasiswa-->
			<legend><?php echo $button ?> Anggota</legend>		 
			<form role="form" class="form-horizontal"  action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<input type="hidden"  class="form-control" name="photo" id="photo" value="<?php echo $photo; ?>" />
				<input type="hidden"  class="form-control" name="id_mahasiswa" id="id_mahasiswa" value="<?php echo $id_mahasiswa; ?>" />
				<input type="hidden"  class="form-control" name="lastedit_date" id="lastedit_date" value="<?php echo $lastedit_date; ?>" />
				<div class="form-group">
					<label class="col-sm-2" for="char">NIM</label>
					<div class="col-sm-4">
						<input type="text"   class="form-control" name="nim" id="nim" placeholder="Nim" value="<?php echo $nim; ?>" readonly />
						<?php echo form_error('nim'); ?>
					</div>
				</div>

				<div class="form-group"> 
					<label class="col-sm-2" for="int">Status Anggota </label>
					<div class="col-sm-4">
						<?php 
							   $username    = $this->session->userdata['username'];	
							   $query = $this->db->query('SELECT konfigurasi_peminjaman.id_peminjam, 
								 konfigurasi_peminjaman.deskripsi_peminjam FROM konfigurasi_peminjaman, 
								 mahasiswa WHERE mahasiswa.nim = ' . $username . ' 
								 AND mahasiswa.id_peminjam = konfigurasi_peminjaman.id_peminjam');
							   $dropdowns = $query->result();
							   
							   foreach($dropdowns as $dropdown) {									
						?>			
									<input type="text" class="form-control"  placeholder="Status Anggota" value="<?php echo $dropdown->deskripsi_peminjam; ?>" readonly />
									<input type="text" name="id_peminjam" id="id_peminjam" placeholder="Status Anggota" value="<?php echo $dropdown->id_peminjam; ?>" hidden />	
						<?php									
									}
								 
							  echo form_error('id_peminjam') 
						?> 
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-2" for="char">ID Chat Telegram
					<img src="<?php echo base_url()?>../assets/images/questions-circular-button.png"
					title="Silahkan Chat /start di bot untuk mengetahui ID Chat">
					</label>
					<div class="col-sm-4">
						<input type="text"   class="form-control" name="id_telegram" id="id_telegram" placeholder="id Telegram" value="<?php echo $id_telegram; ?>" />
						<?php echo form_error('nim'); ?>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2" for="varchar">Nama Lengkap</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo $nama_lengkap; ?>" />
						<?php echo form_error('nama_lengkap') ?>
					</div>
				</div>	
						
				<div class="form-group">
					<label class="col-sm-2"  for="varchar">Alamat </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat" value="<?php echo $alamat; ?>" />
						<?php echo form_error('alamat') ?>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-2" for="varchar">Email </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
						<?php echo form_error('email') ?>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-2" for="varchar">Telp </label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="telp" id="telp" placeholder="Telp" value="<?php echo $telp; ?>" />
						<?php echo form_error('telp') ?>
					</div>
				</div>
					
				<div class="form-group">        
					<label class="col-sm-2" for="varchar">Tempat Lahir </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo $tempat_lahir; ?>" />
						<?php echo form_error('tempat_lahir') ?>
					</div>
				</div>
					
				<div class="form-group">         
					<label class="col-sm-2" for="date">Tanggal Lahir </label>  	
					<div class="col-sm-4">
						<input type="date" class="form-control" name="tgl_lahir" value="<?php echo isset($tgl_lahir) ? set_value('tgl_lahir', date('Y-m-d', strtotime($tgl_lahir))) : set_value('tgl_lahir'); ?>">
						<?php echo form_error('tgl_lahir') ?>	
					</div>
				</div>
					
				<div class="form-group">         
					<label class="col-sm-2" for="enum">Jenis Kelamin</label>
					<div class="col-sm-4">
						<?php 
							$pilihan = array("" => "-- Pilihan --","L" => "Laki-laki", "P" => "Perempuan");
							echo form_dropdown('jenis_kelamin', $pilihan,$jenis_kelamin, 'class="form-control" id="jenis_kelamin"'); 
							echo form_error('jenis_kelamin'); 
						?>		 
					</div>
				</div>
					
					 
				<div class="form-group"> 
					<label class="col-sm-2" for="int">Program Studi </label>
					<div class="col-sm-4">
						<?php 
							   $username    = $this->session->userdata['username'];	
							   $query = $this->db->query('SELECT prodi.id_prodi, prodi.nama_prodi FROM prodi, mahasiswa WHERE mahasiswa.nim = ' . $username . ' AND mahasiswa.id_prodi = prodi.id_prodi');
							   $dropdowns = $query->result();
							   
							   foreach($dropdowns as $dropdown) {									
						?>			
									<input type="text" class="form-control"  placeholder="Program Studi" value="<?php echo $dropdown->nama_prodi; ?>" readonly />
									<input type="text" name="id_prodi" id="id_prodi" placeholder="Program Studi" value="<?php echo $dropdown->id_prodi; ?>" hidden />	
						<?php									
									}
								 
							  echo form_error('id_prodi') 
						?> 
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2" for="photo">Photo</label>
						<div class="col-sm-4">
							<?php
								if($photo==""){
									echo"<p class='help-block'>Silahkan upload foto Anggota </p>";
								}
								else{
							?>
									<div>			
										<img src="<?php echo base_url()?>../assets/images/<?php echo $photo; ?>">
									</div><br />
							<?php
								}
							?>
							<input type="file" name="photo" id="photo">							
						</div>
	
				</div>
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
					<a href="<?php echo site_url('anggota/read/'.$id_mahasiswa) ?>" class="btn btn-default">Cancel</a>
				</form>  