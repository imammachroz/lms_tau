<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// Deklarasi pembuatan class Judul
class Judul extends CI_Controller
{
     // Konstruktor			
	function __construct()
    {
        parent::__construct();
        $this->load->model('Judul_model'); // Memanggil Judul_model yang terdapat pada models
        $this->load->model('Buku_model');
        $this->load->library('form_validation'); // Memanggil form_validation yang terdapat pada library
		$this->load->helper(array('form', 'url')); // Memanggil form dan url yang terdapat pada helper
		$this->load->library('upload'); // Memanggil upload yang terdapat pada helper
		$this->load->library('datatables'); // Memanggil datatables yang terdapat pada library
    }
	
	// Fungsi untuk menampilkan halaman judul
    public function index(){   

        $this->load->view('judul_list'); // Menampilkan halaman utama judul
        $tmp['content']=$this->load->view('judul_list', TRUE);
		$this->load->view('global/layout',$tmp);	
    }
	
	// Fungsi JSON
	public function json() {
        header('Content-Type: application/json');
        echo $this->Judul_model->json();
	}
	
	

    public function baca_buku($id)
	{
	  // Jika session data username tidak ada maka akan dialihkan kehalaman login			
	  if (!isset($this->session->userdata['username'])) {
		redirect(base_url("login"));
	  }
	  $this->db->select('b.id_buku,b.kode_buku,b.is_ada,j.judul_buku');
	  $this->db->from('buku as b');
	  $this->db->where('b.id_judul', $id);
	  $this->db->join('judul as j','j.id_judul=b.id_judul');
	  $buku = $this->db->get()->result();
	return $buku;
	}
	
	// Fungsi untuk menampilkan halaman judul secara detail
    public function read($id){
		// Jika session data username tidak ada maka akan dialihkan kehalaman login			
		if (!isset($this->session->userdata['username'])) {
			redirect(base_url("login"));
		}
	
		// Menampilkan data berdasarkan id-nya yaitu username
		$rowAdm = $this->Users_model->get_by_id($this->session->userdata['username']);
		$dataAdm = array(	
			'wa'       => 'Web administrator',
			'univ'     => 'Library Management System',
			'username' => $rowAdm->username,
			'email'    => $rowAdm->email,
			'level'    => $rowAdm->level,
			'id_mahasiswa' => $rowAdm->id_mahasiswa,
			'nama_panggilan' => $rowAdm->nama_panggilan,
		);
		
		// Menampilkan data judul yang ada di database berdasarkan id-nya yaitu id_judul
        $row = $this->Judul_model->get_by_id($id);

        $id_judul=$this->input->post('id_judul',TRUE);
        $kode_buku=$this->input->post('kode_buku',true);
        // Menampung data yang diinputkan 	
        $data = array('action' => site_url('judul/judul_action'),
            'id_judul'=>$id_judul,
            );
        
		// Jika data judul tersedia maka akan ditampilkan
        if ($row) {
            $dataBuku=array(
                'button' => 'Create',
                'back'   => site_url('judul'),
                'buku_data'=>$this->baca_buku($id),
                'id_judul'=>$this->Judul_model->get_by_id($id)->id_judul,
                'isbn'=>$this->Judul_model->get_by_id($id)->isbn,			   
                'judul_buku'=>$this->Judul_model->get_by_id($id)->judul_buku,
                'penulis'=>$this->Judul_model->get_by_id($id)->penulis,
                'penerbit'=>$this->Judul_model->get_by_id($id)->penerbit,
                );
            $this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
			$this->load->view('judul/judul_read', $dataBuku); // Menampilkan halaman detail judul
			$this->load->view('footer'); // Menampilkan bagian footer
        } 
		// Jika data judul tidak tersedia maka akan ditampilkan informasi 'Record Not Found'
		else {
			$this->load->view('header', $dataAdm); // Menampilkan bagian header dan object data users
            $this->session->set_flashdata('message', 'Record Not Found');
			$this->load->view('footer'); // Menampilkan bagian footer
            redirect(site_url('judul'));
        }
    }
	
 
}