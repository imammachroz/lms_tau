<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url()?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo $back ?>">Fakultas</a></li>
        <li class="active"><?php echo $button ?> Fakultas</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
		
			<!-- Form input dan edit Fakultas -->
			<legend><?php echo $button ?> Fakultas</legend>	   
			<form action="<?php echo $action; ?>" method="post">
				<div class="form-group">
					<label for="varchar">Kode Fakultas <?php echo form_error('kode_fakultas') ?></label>
					<input type="text" class="form-control" name="kode_fakultas" id="kode_fakultas" placeholder="Kode Fakultas" value="<?php echo $kode_fakultas; ?>" />
				</div>
				<div class="form-group">
					<label for="varchar">Fakultas <?php echo form_error('nama_fakultas') ?></label>
					<input type="text" class="form-control" name="nama_fakultas" id="nama_fakultas" placeholder="Nama Fakultas" value="<?php echo $nama_fakultas; ?>" />
				</div>
				<input type="hidden" name="id_fakultas" value="<?php echo $id_fakultas; ?>" /> 
				<button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
				<a href="<?php echo site_url('fakultas') ?>" class="btn btn-default">Cancel</a>
			</form>  
			<!--// Form Fakultas -->