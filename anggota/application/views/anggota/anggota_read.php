<section class="content-header">
      <h1>
        Library Management System
        <small>Tanri Abeng University</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo $button ?> Anggota</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">        
        <div class="box-body">
				<div class="col-md-12 text-center">
					<div style="margin-top: 4px"  id="message">
						<?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
					</div>
				</div>
				<div>
			<!-- Tampil Data Mahasiswa -->  
			<legend><?php echo $button ?> Anggota</legend>
			<!-- Button untuk melakukan update -->
			<div class="col-md-8 text-left">
			<a href="<?php echo site_url('anggota/update/'.$id_mahasiswa) ?>" class="btn btn-primary">Update</a>	
			<!-- Button cancel untuk kembali ke halaman mahasiswa list -->	
			<a href="<?php echo site_url('anggota') ?>" class="btn btn-warning">Cancel</a>
			</div>
			<div class="col-md-4 text-center">
			<br/>
				<p><i>Last Update :
					<?php echo $lastedit_date; ?></i> </p>
			</div>
			<br/>
			<div>
			 <!-- Menampilkan data mahasiswa secara detail -->
			 <table class="table table-striped table-bordered">
				<tr><td>Photo</td><td><img src="../../../assets/images/<?php echo $photo; ?> "onerror="this.src='<?php echo base_url('assets/img/user-s.png') ?>'"></td></tr>
				<tr><td>Nim</td><td><?php echo $nim; ?></td></tr>
				<tr><td>Status Anggota</td><td><?php echo inputtext('id_peminjam','konfigurasi_peminjaman','deskripsi_peminjam','id_peminjam',$id_peminjam); ?></td></tr>
				<tr><td>ID Telegram</td><td><?php echo $id_telegram; ?></td></tr>
				<tr><td>Nama Lengkap</td><td><?php echo $nama_lengkap; ?></td></tr>
				<tr><td>Alamat</td><td><?php echo $alamat; ?></td></tr>
				<tr><td>Email</td><td><?php echo $email; ?></td></tr>
				<tr><td>Telp</td><td><?php echo $telp; ?></td></tr>
				<tr><td>Tempat Lahir</td><td><?php echo $tempat_lahir; ?></td></tr>
				<tr><td>Tanggal Lahir</td><td><?php echo tgl_indo($tgl_lahir); ?></td></tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>
					<?php 
						if($jenis_kelamin == "L"){
							echo "Laki-laki";
						}
						else{
							echo "Perempuan";
						}
					?>
					</td>
				</tr>
				<tr><td>Fakultas</td><td><?php echo inputtext('id_fakultas','fakultas','nama_fakultas','id_fakultas',$id_fakultas); ?></td></tr>
				<tr><td>Program Studi</td><td><?php echo inputtext('id_prodi','prodi','nama_prodi','id_prodi',$id_prodi); ?></td></tr>
			 </table>
